define(['jquery'], function($, Aviary) {
    window.application = {
        VERSION: '0.0.0-beta',
        ROOT: "/editor/app/",
        WEBSITE_ID: document.getElementsByTagName('html')[0].getAttribute('data-website-id'),
        DOMAIN: document.getElementsByTagName('html')[0].getAttribute('data-base-domain'),
        URL: {
            widget_api: "/user/websites/ajax/widgets/" + document.getElementsByTagName('html')[0].getAttribute('data-website-id'),
            file_api: "/user/websites/ajax/files/" + document.getElementsByTagName('html')[0].getAttribute('data-website-id'),
            page_api: "/user/websites/ajax/pages/" + document.getElementsByTagName('html')[0].getAttribute('data-website-id'),
            design_api: "/user/websites/ajax/design/" + document.getElementsByTagName('html')[0].getAttribute('data-website-id'),
            revision_api: "/user/websites/ajax/revision/" + document.getElementsByTagName('html')[0].getAttribute('data-website-id'),
            media_root: '/websites/' + document.getElementsByTagName('html')[0].getAttribute('data-website-id'),
            color_api: '/api/colorHarmony',
            pixabay_search_api: '/api/pixabay/search',
            pixabay_save_api: '/api/pixabay/save/' + document.getElementsByTagName('html')[0].getAttribute('data-website-id'),
            aviary_save_api: '/user/websites/ajax/aviary/save/' + document.getElementsByTagName('html')[0].getAttribute('data-website-id'),
        },
        HistoryManager: {
            undo: [],
            redo: [],
            flag: 'undo',
            push: function(options) {
                if (this.flag == 'undo') {
                    this.undo.push(options);
                } else {
                    this.flag = 'undo';
                    this.redo.push(options);
                }
            },
            pop: function(flag) {
                if (this.flag == 'undo') {
                    this.flag = 'redo';
                }
                return this[flag].pop();
            },
            clean: function() {
                this.undo = [];
                this.redo = [];
            }
        },
        clipboard: [],
        settings: {
            accepted_file_mime_types: {
                image: ['image/gif', 'image/jpeg', 'image/png'],
                audio: ['audio/mp4', 'audio/mpeg', 'audio/ogg', 'audio/webm', 'audio/vnd.wave'],
                video: ['video/mp4', 'video/ogg', 'video/webm', 'video/x-flv'],
                document: ['application/x-latex', 'application/pdf', 'application/vnd.oasis.opendocument.text', 'application/vnd.oasis.opendocument.spreadsheet', 'application/vnd.oasis.opendocument.presentation', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'],
                misc: ['application/x-7z-compressed', 'application/x-rar-compressed', 'application/x-tar', 'application/zip', 'application/gzip']
            },
            fonts: {
                systemFonts: [{
                    name: "Andale Mono",
                    family: "andale mono,times",
                }, {
                    name: "Arial",
                    family: "arial,helvetica,sans-serif",
                }, {
                    name: "Arial Black",
                    family: "arial black,avant garde",
                }, {
                    name: "Book Antiqua",
                    family: "book antiqua,palatino",
                }, {
                    name: "Comic Sans MS",
                    family: "comic sans ms,sans-serif",
                }, {
                    name: "Courier New",
                    family: "courier new,courier",
                }, {
                    name: "Georgia",
                    family: "georgia,palatino",
                }, {
                    name: "Helvetica",
                    family: "helvetica",
                }, {
                    name: "Impact",
                    family: "impact,chicago",
                }, {
                    name: "Symbol",
                    family: "symbol",
                }, {
                    name: "Tahoma",
                    family: "tahoma,arial,helvetica,sans-serif",
                }, {
                    name: "Terminal",
                    family: "terminal,monaco",
                }, {
                    name: "Times New Roman",
                    family: "times new roman,times",
                }, {
                    name: "Trebuchet MS",
                    family: "trebuchet ms,geneva",
                }, {
                    name: "Verdana",
                    family: "verdana,geneva",
                }, {
                    name: "Webdings",
                    family: "webdings",
                }, {
                    name: "Wingdings",
                    family: "wingdings,zapf dingbats"
                }, ],
                googleFonts: [{
                    name: "Open Sans",
                    family: "Open Sans, sans-serif",
                    loader: 'Open+Sans:400,700,400italic,700italic:latin'
                }, {
                    name: "Josefin Slab",
                    family: "Josefin Slab, serif",
                    loader: 'Josefin+Slab:400,700,400italic,700italic:latin'
                }, {
                    name: "Arvo",
                    family: "Arvo, serif",
                    loader: 'Arvo:400,700,400italic,700italic:latin'
                }, {
                    name: "Lato",
                    family: "Lato, sans-serif",
                    loader: 'Lato:400,700,400italic,700italic:latin'
                }, {
                    name: "Vollkorn",
                    family: "Vollkorn, serif",
                    loader: 'Vollkorn:400,700,400italic,700italic:latin'
                }, {
                    name: "Abril Fatface",
                    family: "Abril Fatface, cursive",
                    loader: 'Abril+Fatface:400,700,400italic,700italic:latin'
                }, {
                    name: "Droid Sans",
                    family: "Droid Sans, sans-serif",
                    loader: 'Droid+Sans:400,700:latin'
                }, {
                    name: "Playfair Display",
                    family: "Playfair Display, serif",
                    loader: 'Playfair+Display:400,700,400italic,700italic:latin'
                }, {
                    name: "Exo 2",
                    family: "Exo 2, sans-serif",
                    loader: 'Exo+2:400,700:latin'
                }, {
                    name: "Luckiest Guy",
                    family: "Luckiest Guy, cursive",
                    loader: 'Luckiest+Guy:400,700,400italic,700italic:latin'
                }, {
                    name: "Fredericka The Great",
                    family: "Fredericka The Great, cursive",
                    loader: 'Fredericka+the+Great::latin'
                }, {
                    name: "Basic",
                    family: "Basic, sans-serif",
                    loader: 'Basic:400,700,400italic,700italic:latin'
                }, {
                    name: "Lobster",
                    family: "Lobster, cursive",
                    loader: 'Lobster:400,700,400italic,700italic:latin'
                }, {
                    name: "Mr De Haviland",
                    family: "Mr De Haviland, cursive",
                    loader: 'Mr+De+Haviland:400,700,400italic,700italic:latin'
                }, {
                    name: "Spinnaker",
                    family: "Spinnaker, sans-serif",
                    loader: 'Spinnaker:400,700,400italic,700italic:latin'
                }, {
                    name: "Notice Text",
                    family: "Notice Text, cursive",
                    loader: 'Notice+Text:400,700,400italic,700italic:latin'
                }, {
                    name: "Jockey One",
                    family: "Jockey One, cursive",
                    loader: 'Jockey+One:400,700,400italic,700italic:latin'
                }, {
                    name: "Enriqueta",
                    family: "Enriqueta, sans-serif",
                    loader: 'Enriqueta:400,700,400italic,700italic:latin'
                }, {
                    name: "Anton",
                    family: "Anton, cursive",
                    loader: 'Anton:400,700,400italic,700italic:latin'
                }, {
                    name: "Play",
                    family: "Play, sans-serif",
                    loader: 'Play:400,700,400italic,700italic:latin'
                }, ],
            },
            site_dafault_backgrounds_preset: [{
                name: 'symphony',
                id: "b1",
                styles: {
                    background_image: 'url(/editor/assets/images/symphony.png)',
                    background_size: 'auto',
                    background_position: '50% 0px',
                    background_repeat: 'repeat',
                    background_attachment: 'fixed'
                },
                thumbnail: '/editor/assets/images/symphony.png'
            }, {
                name: 'restaurant',
                id: "b2",
                styles: {
                    background_image: 'url(/editor/assets/images/restaurant.png)',
                    background_size: 'auto',
                    background_position: '50% 0px',
                    background_repeat: 'repeat',
                    background_attachment: 'fixed'
                },
                thumbnail: '/editor/assets/images/restaurant.png'
            }, {
                name: 'subtle',
                id: "b3",
                styles: {
                    background_image: 'url(/editor/assets/images/subtle.png)',
                    background_size: 'auto',
                    background_position: '50% 0px',
                    background_repeat: 'repeat',
                    background_attachment: 'fixed'
                },
                thumbnail: '/editor/assets/images/subtle.png'
            }, {
                name: 'swirl',
                id: "b4",
                styles: {
                    background_image: 'url(/editor/assets/images/swirl.png)',
                    background_size: 'auto',
                    background_position: '50% 0px',
                    background_repeat: 'repeat',
                    background_attachment: 'fixed'
                },
                thumbnail: '/editor/assets/images/swirl.png'
            }, {
                name: 'upfeathers',
                id: "b5",
                styles: {
                    background_image: 'url(/editor/assets/images/upfeathers.png)',
                    background_size: 'auto',
                    background_position: '50% 0px',
                    background_repeat: 'repeat',
                    background_attachment: 'fixed'
                },
                thumbnail: '/editor/assets/images/upfeathers.png'
            }],
            site_dafault_color_preset: [{
                name: 'Lets Hear It For The Boys',
                colors: ['#818181', '#fc6000', '#666666', '#5e93c4', '#ec9712'],
                id: 'c1',
            }, {
                name: 'I See Red',
                colors: ['#cc1013', '#828282', '#ffe900', '#828282', '#0da8e0'],
                id: 'c2',
            }, {
                name: 'Im a Bumble Bee',
                colors: ['#f7b800', '#000000', '#c45033', '#c68733', '#846374'],
                id: 'c3',
            }, {
                name: 'Licorice Allsorts In Blue',
                colors: ['#009ed8', '#747575', '#79af13', '#7f7f7f', '#f79a0e'],
                id: 'c4',
            }, {
                name: 'Pretty In Pink',
                colors: ['#b98580', '#989897', '#d7c0af', '#d68624', '#b59d5b'],
                id: 'c5',
            }, {
                name: 'Tranquil Sea',
                colors: ['#809a91', '#92a6a2', '#887757', '#a1c460', '#ff3d81'],
                id: 'c6',
            }, {
                name: 'My Blue Sneakers',
                colors: ['#cfcfcf', '#00b9e8', '#d32300', '#89bf24', '#ffd000'],
                id: 'c7',
            }, {
                name: 'Chefs Like Orange',
                colors: ['#dda92e', '#cc0808', '#5a5f67', '#ef5310', '#90904b'],
                id: 'c8',
            }, {
                name: 'Corporate Blues',
                colors: ['#6ea4ca', '#1b99e8', '#5c697b', '#5a7972', '#615d7b'],
                id: 'c9',
            }, {
                name: 'At The Car Wash',
                colors: ['#ed8600', '#6b7986', '#576978', '#666666', '#e85a0d'],
                id: 'c10',
            }, ],
            site_dafault_font_preset: [{
                id: "f1",
                name: "Fredericka The Great",
                fonts: {
                    title: {
                        font_family: "Fredericka The Great, cursive",
                        font_size: "80px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    menu: {
                        font_family: "Basic, sans-serif",
                        font_size: "15px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    page_title: {
                        font_family: "Fredericka The Great, cursive",
                        font_size: "29px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_xl: {
                        font_family: "Fredericka The Great, cursive",
                        font_size: "50px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_l: {
                        font_family: "Basic, sans-serif",
                        font_size: "30px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_m: {
                        font_family: "Fredericka The Great, cursive",
                        font_size: "23px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_s: {
                        font_family: "Fredericka The Great, cursive",
                        font_size: "18px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_l: {
                        font_family: "Basic, sans-serif",
                        font_size: "15px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_m: {
                        font_family: "Basic, sans-serif",
                        font_size: "13px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_s: {
                        font_family: "Basic, sans-serif",
                        font_size: "12px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_xs: {
                        font_family: "Basic, sans-serif",
                        font_size: "10px",
                        font_weight: "normal",
                        font_style: "normal"
                    }
                }
            }, {
                id: "f2",
                name: "Lobster",
                fonts: {
                    title: {
                        font_family: "Lobster, cursive",
                        font_size: "80px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    menu: {
                        font_family: "Open Sans, sans-serif",
                        font_size: "15px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    page_title: {
                        font_family: "Lobster, cursive",
                        font_size: "29px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_xl: {
                        font_family: "Lobster, cursive",
                        font_size: "50px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_l: {
                        font_family: "Open Sans, sans-serif",
                        font_size: "30px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_m: {
                        font_family: "Lobster, cursive",
                        font_size: "23px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_s: {
                        font_family: "Lobster, cursive",
                        font_size: "18px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_l: {
                        font_family: "Open Sans, sans-serif",
                        font_size: "15px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_m: {
                        font_family: "Open Sans, sans-serif",
                        font_size: "13px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_s: {
                        font_family: "Open Sans, sans-serif",
                        font_size: "12px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_xs: {
                        font_family: "Open Sans, sans-serif",
                        font_size: "10px",
                        font_weight: "normal",
                        font_style: "normal"
                    }
                }
            }, {
                id: "f3",
                name: "Mr De Haviland",
                fonts: {
                    title: {
                        font_family: "Mr De Haviland, cursive",
                        font_size: "80px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    menu: {
                        font_family: "Spinnaker, sans-serif",
                        font_size: "15px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    page_title: {
                        font_family: "Notice Text, cursive",
                        font_size: "29px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_xl: {
                        font_family: "Notice Text, cursive",
                        font_size: "50px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_l: {
                        font_family: "Spinnaker, sans-serif",
                        font_size: "30px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_m: {
                        font_family: "Notice Text, cursive",
                        font_size: "23px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_s: {
                        font_family: "Notice Text, cursive",
                        font_size: "18px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_l: {
                        font_family: "Spinnaker, sans-serif",
                        font_size: "15px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_m: {
                        font_family: "Spinnaker, sans-serif",
                        font_size: "13px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_s: {
                        font_family: "Spinnaker, sans-serif",
                        font_size: "12px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_xs: {
                        font_family: "Spinnaker, sans-serif",
                        font_size: "10px",
                        font_weight: "normal",
                        font_style: "normal"
                    }
                }
            }, {
                id: "f4",
                name: "Jockey One",
                fonts: {
                    title: {
                        font_family: "Jockey One, cursive",
                        font_size: "80px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    menu: {
                        font_family: "Enriqueta, sans-serif",
                        font_size: "15px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    page_title: {
                        font_family: "Enriqueta, cursive",
                        font_size: "29px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_xl: {
                        font_family: "Enriqueta, cursive",
                        font_size: "50px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_l: {
                        font_family: "Enriqueta, sans-serif",
                        font_size: "30px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_m: {
                        font_family: "Enriqueta, cursive",
                        font_size: "23px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_s: {
                        font_family: "Jockey One, cursive",
                        font_size: "18px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_l: {
                        font_family: "Enriqueta, sans-serif",
                        font_size: "15px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_m: {
                        font_family: "Enriqueta, sans-serif",
                        font_size: "13px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_s: {
                        font_family: "Enriqueta, sans-serif",
                        font_size: "12px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_xs: {
                        font_family: "Jockey One, sans-serif",
                        font_size: "10px",
                        font_weight: "normal",
                        font_style: "normal"
                    }
                }
            }, {
                id: "f4",
                name: "Jockey One",
                fonts: {
                    title: {
                        font_family: "Anton, cursive",
                        font_size: "80px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    menu: {
                        font_family: "Play, sans-serif",
                        font_size: "15px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    page_title: {
                        font_family: "Play, cursive",
                        font_size: "29px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_xl: {
                        font_family: "Play, cursive",
                        font_size: "50px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_l: {
                        font_family: "Play, sans-serif",
                        font_size: "30px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_m: {
                        font_family: "Play, cursive",
                        font_size: "23px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    heading_s: {
                        font_family: "Play cursive",
                        font_size: "18px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_l: {
                        font_family: "Play, sans-serif",
                        font_size: "15px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_m: {
                        font_family: "Play, sans-serif",
                        font_size: "13px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_s: {
                        font_family: "Play, sans-serif",
                        font_size: "12px",
                        font_weight: "normal",
                        font_style: "normal"
                    },
                    body_xs: {
                        font_family: "Play sans-serif",
                        font_size: "10px",
                        font_weight: "normal",
                        font_style: "normal"
                    }
                }
            }, ],
        },
        app: {
            // ImageEditor: new Aviary.Feather({
            //     apiKey: '88d348e084a6a79c',
            //     apiVersion: 3,
            //     onError: function(err) {
            //         console.log(err);
            //     },
            //     onLoad: $.proxy(function() {
            //         application.flags.image_editor = true;
            //     }, this),
            // })
        },
        flags: {
            image_editor: false,
        }
    }
    return window.application;
})
