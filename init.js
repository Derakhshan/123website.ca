// init.js

///
// 
//	This file contains all configurations, constants and difinitions needed in the application
//	
///

// All the libs we use frequently should be addressed here
require.config({
    waitSeconds: 120,
    paths: {
        jquery: "libs/jquery/dist/jquery",
        'jquery-ui': "libs/jquery.ui/ui",
        underscore: "libs/underscore/underscore",
        backbone: "libs/backbone/backbone",
        interact: "libs/interact/interact",
        notify_core: 'libs/notifyjs/dist/notify-combined',
        notify: 'libs/notifyjs/dist/styles/metro/notify-metro',
        tinymce: 'libs/tinymce/tinymce.min',
        aviary: 'http://feather.aviary.com/js/feather',
        webfontloader: 'libs/webfontloader/webfontloader',
        mousetrap: 'libs/mousetrap/mousetrap',
        templates: "src/helpers/templates/init",
        base: "src/components/base/",
        widget: "src/components/base/widget/init",
        editor: "src/components/editor/",
        text: "src/components/text/",
        image: "src/components/image/",
        gallery: "src/components/gallery/",
        menu: "src/components/menu/",
        line: "src/components/line/",
        page: "src/components/page/",
        file_manager: "src/components/base/file_manager/init",
        color_selector: "src/components/base/color_selector/init",
    },
    shim: {
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        notify_core: {
            deps: ['jquery'],
            exports: '$.notify',
        },
        notify: {
            deps: ['jquery', 'notify_core'],
            exports: '$.notify'
        },
        iframeUpload: {
            deps: ['jquery'],
            exports: '$.fn.ajax',
        },
        tinymce: {
            exports: 'tinymce',
            init: function() {
                this.tinymce.DOM.events.domLoaded = true;
                return this.tinymce;
            }
        },
        aviary: {
            exports: "window.Aviary"
        },
        webfontloader: {
            exports: "window.WebFont"
        }
    }
});

require([
    'app',
    'editor/init',
    'underscore',
    'libs/hogan/web/builds/3.0.2/hogan-3.0.2.amd',
    'templates.bin',
], function(application, Editor, _, Hogan, Aviary) {
    "user strict";
    //	Pre-Compile all the templates to boost up creation of elements
    _.each(application.templates, function(value, key) {
        application.templates[key] = Hogan.compile(value);
    });

    //	Initiating the application by calling Editor constructor
    var editor = new Editor;
    editor.testWidgets();
});
