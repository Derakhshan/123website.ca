
define([
	'jquery',
	'underscore',
	'backbone',
	'templates',
	'widget',
	'src/components/audio/views/audio'
], function ($, _, Backbone, Templates, Widget, View) {
	return Widget.extend({
		contentView: new View()
		width: 200,
	});
});