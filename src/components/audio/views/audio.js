define([
	'jquery',
	'underscore',
	'backbone',
	'templates',
	'interact',
], function ($, _, Backbone, Template, Interact) {
	return Backbone.View.extend({
		tagName: 'div',
		className: 'text-widget-wraper',
		initialize: function () {
			this.$el.html();
		}
	});
});