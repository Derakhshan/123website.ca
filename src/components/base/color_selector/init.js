/**
 *		COLOR SELECTOR
 *		VERSION: 0.0.1
 *
 * 		This class provides a layout and functionality for file color selection
 * 		
 *		@param
 *			String				Stipulates color selector mode (mini, full)
 *			Function 			A callback function
 * 		@dependencies:
 * 			Backbone 			Uses backbone views
 *			FlexiColorPicker	Uses a a visual aid for user to choose color
 *			Dialog				Jquery Ui dialog as the main panel
 * 		@views
 * 			color 				A Backbone View, containing color selection tools
 * 		@models
 * 			Design			 	A Backbone model, containing website color settings
 * 
 */

define([
	'base/color_selector/views/mini',
	'base/color_selector/views/full'
], function (MiniColorPicker, FullColorPicker) {
	return function (mode, callback, event) {
		if (mode === 'mini') {
			//	Construct the mini color picker view
			var view = new MiniColorPicker({
				model: application.design
			});

			//	Position of dialog
			console.log(event.currentTarget, event);
			if (event) {
				var position = {
					my: "left top",
					at: "left top",
					of: event.target
				}
			}
			else {
				var position = {
					my: "center",
					at: "center",
					of: window
				}
			}

			//	Mini color picker opens as a dialog
			view.render().$el.dialog({
				title: "Color Picker",
				width: 170,
				height: 230,
				position: position,
				closeOnEscape: true,
				buttons: [{ 
					text: "Select", 
					click: function(event) { 
						callback(view.$el.find('a.focus').attr('data-class-name'));
						view.$el.dialog('close');
					}
				}, {
					text: "Remove",
					click: function (event) {
						callback('color_transparent');
						view.$el.dialog('close');
					}
				}],
				close: function (event) {
					$(this).dialog('destroy');
				}
			});

			return view;
		}
		else {
			view = new FullColorPicker({
				model: application.design
			});

			return view;
		}
	}
});
