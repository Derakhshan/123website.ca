/**
 *		COLOR PICKER VIEW
 *		VERSION: 0.0.1
 *
 * 		This class provides the view for color selector app
 * 		
 *		@param
 *			String				Choses between mini mode and full mode
 * 		@dependencies:
 * 			Backbone 			Uses backbone collection as a container for file instances
 * 			FlexiColorPicker 	A library to handle color selection widget
 * 		@models
 * 			Design	 			A Backbone model, used to store site design options
 * 
 */
define([
	'jquery',
	'underscore',
	'backbone',
	'templates',
	'libs/colorpicker/colorpicker',
], function ($, _, Backbone, Template, ColorPicker) {
	return Backbone.View.extend({
		tagName: 'div',
		className: 'color-picker-panel',
		events: {
			'contextmenu': function (event) {
				//	Prevent context menu propagation
				event.preventDefault();
				event.cancelBubble = true;
				if (event.stopPropagation) {
					 event.stopPropagation();	
				}
			},
			'click a': function (event) {
				//	Give the chosen color focus class
				this.$el.find('a.focus').removeClass('focus');
				$(event.currentTarget).addClass('focus');

				//	If it's a main color open the color picker, else close color picker
				if ( $(event.currentTarget).hasClass('main-color-selector-btn') ) {
					this.$el.find('.color-picker-container').slideDown();
				}
				else {
					this.$el.find('.color-picker-container').slideUp();
				}
			},
		},
		initialize: function (options) {
			this.options = options;
		},
		render: function () {
			//	Construct color picker template
			this.$el.html(Template('color_selector', 'color_picker', {}));

			//	initiate color picker
			this.$el.find('.color-picker-container').ColorPicker(this.options);

			//	Delete colorpicker_submit button
			this.$el.find('.colorpicker_submit').remove();

			return this;
		}
	});
});