/**
 *		COLOR SELECTOR VIEW
 *		VERSION: 0.0.1
 *
 * 		This class provides the view for color selector app
 * 		
 *		@param
 *			String				Choses between mini mode and full mode
 * 		@dependencies:
 * 			Backbone 			Uses backbone collection as a container for file instances
 * 		@models
 * 			Design	 			A Backbone model, used to store site design options
 * 
 */
define([
	'jquery',
	'underscore',
	'backbone',
	'templates',
	'base/color_selector/views/color_picker',
	'jquery-ui/dialog'
], function ($, _, Backbone, Template, ColorPicker, Dialog) {
	return Backbone.View.extend({
		tagName: 'div',
		className: 'color-selector-panel',
		events: {
			'contextmenu': function (event) {
				//	Prevent context menu propagation
				event.preventDefault();
				event.cancelBubble = true;
				if (event.stopPropagation) {
					 event.stopPropagation();	
				}
			},
			'click a.full-color-selector-btn': function (event) {
				//	Get target color
				var raw_color = /rgb\((\d+), (\d+), (\d+)\)/g.exec(event.target.style.backgroundColor),
					color = {
						r: raw_color[1],
						g: raw_color[2],
						b: raw_color[3]
					},
					target_class = $(event.currentTarget).attr('data-class-name'),
					target_color = this.currentColors[target_class];

				//	Construct color picker
				var color_picker = new ColorPicker({
					color: color,
					flat: true,
					onChange: $.proxy(function (hsb, hex, rgb, el) {
						this.currentColors[target_class] = '#' + hex;

						//	If a main color has been changed then update the pallete
						if ($(event.target).hasClass('full-color-main')) {
							this.update();
						}
						else {
							this.render();
						}
					}, this)
				});

				var select = $.proxy(function () {
					target_color = this.currentColors[target_class];
					color_picker.$el.dialog('close');
				}, this);

				var close = $.proxy(function () {
					this.currentColors[target_class] = target_color;
					//	If a main color has been changed then update the pallete
					if ($(event.target).hasClass('full-color-main')) {
						this.update();
					}
					else {
						this.render();
					}
					color_picker.$el.dialog('destroy');
				}, this);

				color_picker.render().$el.dialog({
					title: "Color Picker",
					width: 400,
					closeOnEscape: true,
					buttons: [{ 
						text: "Select",
						click: select
					}],
					close: close
				});

			},
		},
		initialize: function () {
			this.currentColors = _.clone(this.model.get('color_settings'));
		},
		update: function () {
			//	On change of color we need to update or color plate
			var mainColors = _.filter(this.currentColors, function (color, className) {
				if (className == "color_13" || className == "color_18" || className == "color_23" || className == "color_28" || className == "color_33") {
					return true;
				}
				else {
					return false;
				}
			}).join(',');

			$.ajax({
				context: this,
				method: "GET",
				url: application.URL.color_api,
				data: {
					mainColors: mainColors
				},
				success: function (data) {
					//	We should update the color data in a manner which the 10 first color stay put
					_.each(data, function (value, index) {
						if (parseInt(parseInt(/(\d+)/.exec(index)[1]))>10) {
							this.currentColors[index] = value;
						}
					}, this);

					var style = $('head').find('style#colorStyle')
					_.each(this.currentColors, function (color, className) {
						style.append("." + className + " { color: " + color + "!important }");
						style.append(".bg_" + className + " { background-color: " + color + "!important }")
						style.append(".border_" + className + " { border-color: " + color + "!important }")
					});
					this.render();
				}
			});
		},
		render: function () {
			//	Construct color platte manifest
			var platte = _.map(this.currentColors, function (color, className) {
				return {
					className: className,
					color: color
				}
			}),

			//	Categorize the color palate with main colors, extracted colors and more colors
			param = {
				main_colors: [platte[13], platte[18], platte[23], platte[28], platte[33]],
				extracted_colors: [
					platte.slice(11,16),
					platte.slice(16,21),
					platte.slice(21,26),
					platte.slice(26,31),
					platte.slice(31,36),
				],
				more_colors: platte.slice(1,6),
			}
			this.$el.html(Template('color_selector', 'full', param));

			return this;
		}
	});
});