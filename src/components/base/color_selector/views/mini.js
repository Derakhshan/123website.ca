/**
 *		COLOR SELECTOR VIEW
 *		VERSION: 0.0.1
 *
 * 		This class provides the view for color selector app
 * 		
 *		@param
 *			String				Choses between mini mode and full mode
 * 		@dependencies:
 * 			Backbone 			Uses backbone collection as a container for file instances
 * 			FlexiColorPicker 	A library to handle color selection widget
 * 		@models
 * 			Design	 			A Backbone model, used to store site design options
 * 
 */
define([
	'jquery',
	'underscore',
	'backbone',
	'templates',
], function ($, _, Backbone, Template) {
	return Backbone.View.extend({
		tagName: 'div',
		className: 'color-selector-panel',
		events: {
			'contextmenu': function (event) {
				//	Prevent context menu propagation
				event.preventDefault();
				event.cancelBubble = true;
				if (event.stopPropagation) {
					 event.stopPropagation();	
				}
			},
			'click a': function (event) {
				//	Give the chosen color focus class
				this.$el.find('a.focus').removeClass('focus');
				$(event.currentTarget).addClass('focus');

				//	If it's a main color open the color picker, else close color picker
				if ( $(event.currentTarget).hasClass('main-color-selector-btn') ) {
					this.$el.find('.color-picker-container').slideDown();
				}
				else {
					this.$el.find('.color-picker-container').slideUp();
				}
			},
		},
		initialize: function () {
		},
		render: function () {
			//	Construct color plate manifest
			var plate = _.map(this.model.get('color_settings'), function (color, className) {
				return {
					className: className,
					color: color
				}
			}),

			//	Categorize the color palate with main colors, extracted colors and more colors
			param = {
				colors: [
					plate.slice(11,16),
					plate.slice(16,21),
					plate.slice(21,26),
					plate.slice(26,31),
					plate.slice(31,36),
					plate.slice(1,6),
				],
			}
			this.$el.html(Template('color_selector', 'mini', param));

			return this;
		}
	});
});