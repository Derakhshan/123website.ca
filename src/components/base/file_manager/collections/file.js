/**
 *		FILE COLLECTION
 *		VERSION: 0.0.1
 *
 * 		This class provides file collection to be used in file management
 * 		
 * 		@dependencies:
 * 			Backbone 	Uses backbone collection as a container for file instances
 * 
 */
define([
	'backbone',
	'base/file_manager/models/file'
], function (Backbone, File) {
	return Backbone.Collection.extend({
		model: File
	});
});