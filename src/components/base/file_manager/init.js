/**
 *		FILE MANAGER
 *		VERSION: 0.0.1
 *
 * 		This class provides a layout and functionality for file management, including upload, delete, view
 * 		
 *   	@param
 *   		String		Stipulates the the acceptable mime type
 * 		@dependencies:
 * 			Backbone 	Uses backbone collection as a container for file instances
 * 		@views
 * 			Text 	A Backbone View, manages the main content of widget
 * 			Setting A Backbone View, manages the main settings of text widget
 * 		@models
 * 			Main 	A Backbone model, containing main features of text widget 
 * 
 */

define([
	'base/file_manager/collections/file',
	'base/file_manager/views/files'
], function (FileCollection, Files) {
	return function (type, callback) {
		//	Acceptable mime types
		this.types = {
			image: ['image/gif', 'image/jpeg', 'image/png'],
			audio: ['audio/mp4', 'audio/mpeg', 'audio/ogg', 'audio/webm', 'audio/vnd.wave'],
			video: ['video/mp4', 'video/ogg', 'video/webm', 'video/x-flv'],
			document: ['application/x-latex', 'application/pdf', 'application/vnd.oasis.opendocument.text', 'application/vnd.oasis.opendocument.spreadsheet', 'application/vnd.oasis.opendocument.presentation', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'],
			misc: ['application/x-7z-compressed', 'application/x-rar-compressed', 'application/x-tar', 'application/zip', 'application/gzip']
		}

		this.files = new FileCollection();

		this.view = new Files({collection: this.files, type: type});

		var select = $.proxy(function(event) { 
			if ( this.view.$el.find('li.selected').attr('data-file-src') ) {
				callback(this.view.$el.find('li.selected').attr('data-file-src'));
				this.view.$el.dialog('close');
			}
			else if ( this.view.$el.find('li.selected').attr('data-file-api') == 'pixabay' ) {
				$.ajax({
					context: this,
					method: 'POST',
					url: application.URL.pixabay_save_api,
					data: {webformatURL: this.view.$el.find('li.selected').attr('data-webformat-url')},
					dataType: 'json',
					success: function (data) {
						callback(data.file.src);
						this.view.$el.dialog('close');
					}
				})
			}
		}, this);

		this.view.render().$el.dialog({
			closeOnEscape: true,
			minWidth: 750,
			buttons: [{ 
				text: "Select File", 
				click: select
			}],
			close: function (event) {
				$(this).dialog('destroy');
			}
		});
	}
});
