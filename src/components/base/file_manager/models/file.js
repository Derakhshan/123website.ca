/**
 *		FILE MODEL
 *		VERSION: 0.0.1
 *
 * 		This class provides file Model to be used in file management
 * 		
 * 		@dependencies:
 * 			Backbone 	Uses backbone model as a container for file instance
 * 
 */
define([
	'backbone',
], function (Backbone, File) {
	return Backbone.Model.extend({
		defaults: {
			id: '',
			filename: '',
			size: '',
			mimetype: '',
			title: ''
		}
	});
});