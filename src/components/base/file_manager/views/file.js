/**
 *		FILE VIEW
 *		VERSION: 0.0.1
 *
 * 		This class provides the layout for a single file to be used in file manager app
 * 		
 * 		@dependencies:
 * 			Backbone 	Uses backbone collection as a container for file instances
 * 			FileDrop 	A library to handle ajax upload and fallback for older browsers
 * 		@models
 * 			Files	 	A Backbone collection, containing file instances 
 * 
 */
define([
	'backbone',
	'templates',
], function (Backbone, Template) {
	return Backbone.View.extend({
		tagName: 'li',

		className: 'file-manager-panel',

		events: {
			'click': function () {
				$('li.selected').removeClass('selected');
				this.$el.addClass('selected');
			}
		},

		initialize: function () {
			//	If pixabay api
			if (this.model.get('mimetype') == "pixabay/api") {
				this.$el.attr('data-file-api', 'pixabay');
				this.$el.attr('data-webformat-url', this.model.get('webformatURL'));
			}
			else {
				this.$el.attr('data-file-id', this.model.get('id'));
				this.$el.attr('data-file-src', this.model.get('src'));
			}
		},

		render: function () {
			//	Find out file type using mime type
			_.each(application.settings.accepted_file_mime_types, function (types, className) {
				if (_.contains(types, this.model.get('mime_type'))) {
					this.file_class = className;
				}
			}, this);

			//	Not listed mimetype, maybe api
			if ( !this.file_class) {
				this.file_class = this.model.get('mimetype');
			}

			//	Construct view using the right icon and thumbnails
			var param = {
				title: this.model.get('title')
			}

			switch (this.file_class) {
				case "image":
					param['thumbnail_src'] = this.model.get('src');
					break;
				case "pixabay/api":
					param['thumbnail_src'] = this.model.get('src');
					break;
			}

			console.log(param, this.file_class);
			this.$el.html(Template('file_manager', 'file', param));			
			return this;
		}
	});
});