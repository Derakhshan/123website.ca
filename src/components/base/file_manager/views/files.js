/**
 *		FILE COLLECTION VIEW
 *		VERSION: 0.0.1
 *
 * 		This class provides the layout for file manager app
 * 		
 * 		@dependencies:
 * 			Backbone 	Uses backbone collection as a container for file instances
 * 			FileDrop 	A library to handle ajax upload and fallback for older browsers
 * 		@models
 * 			Files	 	A Backbone collection, containing file instances 
 * 
 */
define([
	'backbone',
	'libs/jquery.iframe-transport/jquery.iframe-transport',
	'templates',
	'base/file_manager/views/file'
], function (Backbone, IframeUpload, Template, File) {
	return Backbone.View.extend({
		tagName: 'div',
		className: 'file-manager-panel',
		events: {
			'change #file': function (event) {
				$.ajax({
					context: this,
					method: 'POST',
					accepts: "application/json",
					dataType: "json",
					url: application.URL.file_api,
					files: this.$el.find('#file'),
					iframe: true,
					success: function (data) {
						if (data.status == true ) {
							this.collection.add(data.file);
							this.render();
						}
						else {
							console.log('some error has happend!', data.error);
						}
					}
				});
			},
			'click a.file-manager-upload-file-btn': function (event) {
				this.$el.find('input#file').click();
			},
			'click a#file-manager-local-files': function (event) {
				this.collection.reset();
				this.$el.find('.file-manager-search-input-container').hide();
				this.getLocalFiles();
			},
			'click a#file-manager-pixabay-files': function (event) {
				this.collection.reset()
				this.render();
				this.$el.find('.file-manager-search-input-container').show();
				this.$el.find('#file-manager-search-input').on('change', $.proxy(function (event) {
					this.collection.reset()
					this.pixabay_search(this.$el.find('#file-manager-search-input').val());
				}, this));
			},
			
		},
		pixabay_search: function (query) {
			$.ajax({
				context: this,
				url: application.URL.pixabay_search_api + '/' + query.split(/\s/).join('+'),
				method: 'GET',
				dataType: "json",
				success: function (data) {
					_.each(data.hits, function (hit) {
						this.collection.add({
							src: hit.previewURL,
							webformatURL: hit.webformatURL,
							mimetype : 'pixabay/api'
						})
					}, this);
					console.log(this.collection);
					this.render();
				}
			});
		},
		getLocalFiles: function () {
			$.ajax({
				context: this,
				method: 'GET',
				url: application.URL.file_api,
				data: {mimeTypes: application.settings.accepted_file_mime_types[this.type].join(',')},
                dataType: "json",
                success: function (data) {
            		this.collection.add(data);
            		this.render();
                }
			});
		},
		initialize: function (options) {
			this.type = options.type;
			this.getLocalFiles();
		},
		render: function () {
			this.$el.html(Template('file_manager', 'files', {}));

			this.collection.each(function (file) {
				this.$el.find('.file-manager-file-list').prepend(new File({model: file}).render().$el);
			}, this);

			return this;
		}
	});
});