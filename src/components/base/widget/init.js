define([
	'jquery',
	'underscore',
	'backbone',
	'templates',
	'base/widget/views/widget',
	'base/widget/views/defaultSetting',
	'base/widget/models/widget',
], function ($, _, Backbone, Templates, View, settingView, Model) {
	return {
		//	Widget Name Space
		VER: '0.0.1-beta',
		extend: function (defaults) {
			return function (options) {
				//	BaseWidget Class				
				//	Factory methods create essential and fundamental elements of widget
				this.modelFactory = defaults.modelFactory;
				this.contentFactory = defaults.contentFactory;
				this.resizeable = defaults.resizeable;

				//	Puts an element in clipboard
				this.copy = $.proxy(function (event) {
					application.clipboard.push({
						type: 'widget',
						data: this.model.toJSON()
					});
				}, this)

				//	Puts an element in clipboard and then deletes it
				this.cut = $.proxy(function (event) {
					application.clipboard.push({
						type: 'widget',
						data: this.model.toJSON()
					});
					this.eliminate(event);
				}, this)

				//	Puts an element in history, then deletes it from dom
				this.eliminate = $.proxy(function (event) {
					// Register action in history
					application.HistoryManager.push({
						type: 'widget',
						action: 'delete',
						data: this.model.toJSON()
					});

					//	Remove view from dom
					this.view.$el.remove();

					//	Destroy the model
					this.model.trigger('destroy');

					//	Dispatch remove event, editor will catch this event and remove this widget from their widget collection
					document.dispatchEvent(this.eventRegistry.deleted, true, true);

				}, this);

				var render = this.render = function () {
					this.view = new View({
						contextMenu: this.contextMenu,
						resizeable: this.resizeable,
						parent: this,
						contentView: this.contentView,
						model: this.model,
						events: this.events,
					}).render();
					return this.view;
				}


				//	Widget Finished event is fired when widget construction is over
				if ( defaults.eventRegistry ) {
					this.eventRegistry = defaults.eventRegistry;
				}
				else {
					this.eventRegistry = {};	
				}
				this.eventRegistry.widgetFinished = document.createEvent('Event');
				this.eventRegistry.widgetFinished.initEvent('widgetFinished', true, true);
				this.eventRegistry.widgetFinished.instance = this;

				this.eventRegistry.changed = document.createEvent('Event');
				this.eventRegistry.changed.initEvent('widgetChanged', true, true);
				this.eventRegistry.changed.instance = this;

				this.eventRegistry.deleted = document.createEvent('Event');
				this.eventRegistry.deleted.initEvent('widgetDeleted', true, true);
				this.eventRegistry.deleted.instance = this;

				if ( defaults.events ) {
					this.events = defaults.events;
				}
				else {
					this.events = {};	
				}

				//	Context Menu
				if ( defaults.contextMenu ) {
					this.contextMenu = defaults.contextMenu;
				}
				else {
					this.contextMenu = [];	
				}

				this.model = new this.modelFactory(options);
				this.contentView = new this.contentFactory({model: this.model});
			} 
		}
	}
})