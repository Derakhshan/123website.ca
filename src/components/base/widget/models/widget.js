//	app/src/components/base/widget/models/widget.js

define([
	'jquery',
	'underscore',
	'backbone',
], function ($, _, Backbone) {
	return Backbone.Model.extend({
		defaults: {
			width: 520,
			height: 500,
			position_top: 200,
			position_left: 400,
			page_id: 0,
			type: 'text',
			showOnAllPages: false,
			settings: {
				position: 'absolute',
				parent: 'body',
				z_index: 0,
				lock: false,
			}
		}
	});
});