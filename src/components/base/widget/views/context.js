/**
 *
 * 		WIDGET CONTEXT MENU VIEW
 * 		VERSION: 0.0.0
 *
 *		Defines the view widgets's context menu
 */

define([
  'jquery',
  'underscore',
  'backbone',
  'templates',
], function ($, _, Backbone, Template, Styles) {
  return Backbone.View.extend({
    tagName: 'div',
    className: 'widget-context-wrapper',
    initialize: function (options) {
      this.events['contextmenu'] = this.events['click'] = function (event) {
        //  Prevent context menu propagation
        event.preventDefault();
        event.cancelBubble = true;
        if (event.stopPropagation) {
           event.stopPropagation(); 
        }
      }

      this.contextMenu = options.contextMenu;
      this.contextMenuFooter = options.contextMenuFooter;
      this.event = options.event;

      this.styles = {
        top: (this.event.clientY + 20) + "px",
        left: (this.event.clientX + 20 ) + "px",
        position: 'fixed',
        zIndex: 1000,
      };
    },
    render : function () {
      //  Constructing the template
      this.$el.html(Template('widget', 'context', {
        contextMenu: this.contextMenu,
        contextMenuFooter: this.contextMenuFooter,
      }));

      //  Applay styles to the element
      this.$el.css(this.styles);

      //  Attach this to the body
      $('body').append(this.$el);

      return this;
    }
  });
});