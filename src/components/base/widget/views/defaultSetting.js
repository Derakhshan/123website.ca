// app/src/components/base/widget/view.js

///
//
//  Creates generic view for base widgets
//
///

define([
  'jquery',
  'underscore',
  'backbone',
  'templates',
], function ($, _, Backbone, Template) {
  return Backbone.View.extend({
    tagName: 'div',
    className: 'modal-wrapper',
    events: {
      "hidden.bs.modal .modal": function () {
        this.model.set('position', [this.$el.find('#y').val(), this.$el.find('#x').val()]);
        this.model.set('size', [this.$el.find('#width').val(), this.$el.find('#height').val()]);
        this.model.set('showOnAllPages', this.$el.find('#showOnAllPages').prop('checked'));
      }
    },
    initialize: function () {
      
    },
    render : function () {
      //  Constructing the template
      var parameters = {
        width: this.model.get('width'),
        height: this.model.get('height'),
        x: this.model.get('left'),
        y: this.model.get('top'),
        sap: this.model.get('showOnAllPages')
      }
      
      this.$el.html(Template('text', 'setting', parameters));
      return this;
    }
  });
});