// app/src/components/base/widget/view.js

///
//
//	Creates generic view for base widgets
//
///

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'interact',
    'jquery-ui/dialog',
    'base/widget/views/context',
    'mousetrap'
], function($, _, Backbone, Template, Interact, Dialog, ContextMenu, MouseTrap) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'widget-base',
        styles: {
            position: 'absolute',
        },
        attributes: {},
        initialize: function(options) {
            //	Save the provided options with in the view
            this.options = options;

            //	Parent
            this.parent = options.parent;

            //	Listener to model change
            this.model.on('change', function(event) {
                this.render();
            }, this);

            //	Listener to editor copy event
            document.addEventListener('editorCopy', $.proxy(function(event) {
                if (this.$el.hasClass('focus')) {
                    this.parent.copy();
                }
            }, this));

            //	Listener to editor cut event
            document.addEventListener('editorCut', $.proxy(function(event) {
                if (this.$el.hasClass('focus')) {
                    this.parent.cut();
                }
            }, this));

            //	Listener to editor delete event
            document.addEventListener('editorDelete', $.proxy(function(event) {
                if (this.$el.hasClass('focus')) {
                    this.parent.eliminate();
                }
            }, this));
        },

        render: function() {
            //	Local Functions
            this.draggable = function() {
                var end = $.proxy(function(event) {
                    var target = this.el;
                    console.log(target);

                    //	Get parent of widget
                    var parent = $(target).parent().prop('tagName').toLowerCase() + ($(target).parent().attr('id') ? ("#" + $(target).parent().attr('id')) : '');
                    if (parent != this.model.get('settings').parent && !(parent == 'div#editorThemeBody' && this.model.get('settings').parent == 'body')) {
                        var settings = _.clone(this.model.get('settings')),
                            position_top = (target.offsetTop + $(settings.parent)[0].offsetTop) - $(target).parent()[0].offsetTop,
                            position_left = (target.offsetLeft + $(settings.parent)[0].offsetLeft) - $(target).parent()[0].offsetLeft;
                        settings.parent = parent;
                        this.model.set({
                            settings: settings,
                            position_top: position_top,
                            position_left: position_left
                        }, {
                            validate: true
                        });
                    } else {
                        this.model.set({
                            position_left: target.getAttribute('data-x'),
                            position_top: target.getAttribute('data-y')
                        }, {
                            validate: true
                        });
                    }

                    //	Update zone
                    $(target).parent().change();
                }, this);
				
				var target = this.el,
					model = this.model;

				var keydown = function(event) {
                    if ($(target).hasClass('focus')) {
                        if (event.keyCode == '38') {
                            var y = (parseFloat(target.getAttribute('data-y')) || 0) - 0.5;
                            target.style.top = y + "px";
                            target.setAttribute('data-y', y);
                        } else if (event.keyCode == '40') {
                            var y = (parseFloat(target.getAttribute('data-y')) || 0) + 0.5;
                            target.style.top = y + "px";
                            target.setAttribute('data-y', y);
                        } else if (event.keyCode == '37') {
                            var x = (parseFloat(target.getAttribute('data-x')) || 0) - 0.5;
                            target.style.left = x + "px";
                            target.setAttribute('data-x', x);
                        } else if (event.keyCode == '39') {
                            var x = (parseFloat(target.getAttribute('data-x')) || 0) + 0.5;
                            target.style.left = x + "px";
                            target.setAttribute('data-x', x);
                        }
                    }
                }
				
				var keyup = function(event) {
                    if ($(target).hasClass('focus')) {
                        if (event.keyCode == '38' || event.keyCode == '40' || event.keyCode == '37' || event.keyCode == '39') {
                        	console.log('test2');
                            model.set({
                                position_left: target.getAttribute('data-x'),
                                position_top: target.getAttribute('data-y')
                            }, {
                                validate: true
                            });
                            console.log(model.hasChanged());
                        }
                    }
                }

                $(document).off('keydown');

                $(document).off('keyup');

                $(document).on('keydown', keydown);

                $(document).on('keyup', keyup);

                return Interact(this.el)
                    .inertia({
                        resistance: 15,
                        zeroResumeDelta: true
                    })
                    .draggable({
                        onmove: function(event) {
                            var target = event.target,
                                x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
                                y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

                            target.style.left = x + "px";
                            target.setAttribute('data-x', x);
                            target.style.top = y + "px";
                            target.setAttribute('data-y', y);
                        },
                        onend: end
                    });
            }

            this.resizable = function() {
                //	Check if the widget is resizeable
                if (this.options.resizeable) {
                    //	If it is only one dimensional resizable
                    if (this.options.resizeable == 'x') {
                        this.$el.find('div.widget-resize-handle[data-vertical-pos="top"], div.widget-resize-handle[data-vertical-pos="bottom"]').remove();
                    } else if (this.options.resizeable == 'y') {
                        this.$el.find('div.widget-resize-handle[data-horizontal-pos="right"], div.widget-resize-handle[data-horizontal-pos="left"]').remove();
                    }

                    var dragend = $.proxy(function(event) {
                        var target = this.el;
                        this.model.set({
                            width: parseInt(target.style.width),
                            height: parseInt(target.style.height),
                            position_top: parseInt(target.style.top),
                            position_left: parseInt(target.style.left),
                        }, {
                            validate: true
                        });
                        $(target).parent().change();
                    }, this);

                    var resize = $.proxy(function(posx, posy, dx, dy) {
                        var wrapper = this.$el.find("div.wrapper div")[0],
                            target = this.el,
                            parent = this.$el.parents('.page-zone')[0],
                            grand_parent = this.$el.parents('.page-zone-wrapper')[0];

                        switch (posy) {
                            case 'top':
                                var newHeight = parseFloat(target.style.height) - dy;
                                target.style.height = newHeight + 'px';
                                wrapper.style.height = newHeight + 'px';
                                target.style.top = (parseFloat(target.style.top) + dy) + 'px';
                                wrapper.style.top = (parseFloat(wrapper.style.top) + dy) + 'px';
                                break;
                            case 'bottom':
                                var newHeight = parseFloat(target.style.height) + dy;
                                //	If the widget's height suppress the parent, make parent higher
                                //	First make sure widget is in zone
                                if (parent && grand_parent && ((target.offsetTop + parseFloat(target.style.height)) < parent.offsetHeight)) {
                                    if (target.offsetTop + newHeight > (parent.offsetHeight - 1)) {
                                        parent.style.height = target.offsetTop + newHeight + 1 + "px";
                                        if (grand_parent.offsetHeight < parent.offsetHeight) {
                                            grand_parent.style.height = parent.offsetHeight + 'px';
                                        }
                                        target.style.height = newHeight + 'px';
                                        wrapper.style.height = newHeight + 'px';
                                    } else {
                                        target.style.height = newHeight + 'px';
                                        wrapper.style.height = newHeight + 'px';
                                    }
                                } else {
                                    target.style.height = newHeight + 'px';
                                    wrapper.style.height = newHeight + 'px';
                                }
                                break;
                        }

                        switch (posx) {
                            case 'right':
                                var newWidth = parseFloat(target.style.width) + dx;
                                target.style.width = newWidth + 'px';
                                wrapper.style.width = newWidth + 'px';
                                break;
                            case 'left':
                                console.log(dy, dx);
                                var newWidth = parseFloat(target.style.width) - dx;
                                target.style.width = newWidth + 'px';
                                wrapper.style.width = newWidth + 'px';
                                target.style.left = (parseFloat(target.style.left) + dx) + 'px';
                                wrapper.style.left = (parseFloat(wrapper.style.left) + dx) + 'px';
                                break;
                        }

                        if (this.model.get('type') == 'image') {
                            if (this.model.get('settings').scaling == 'stretch') {
                                $(wrapper).find('img').css({
                                    width: "100%",
                                    height: "100%"
                                });
                            } else if (this.model.get('settings').scaling == 'crop') {
                                var src = this.model.get('settings').src,
                                    naturalWidth = $(wrapper).find('img')[0].naturalWidth || (function() {
                                        var img = new Image();
                                        img.src = src;
                                        return img.width;
                                    })(),
                                    naturalHeight = $(wrapper).find('img')[0].naturalHeight || (function() {
                                        var img = new Image();
                                        img.src = src;
                                        return img.height;
                                    })();

                                if (wrapper.style.width > wrapper.style.height) {
                                    this.$el.find('img').css({
                                        width: wrapper.style.width,
                                        height: (wrapper.style.width * naturalHeight) / naturalWidth,
                                        marginTop: (wrapper.style.height - ((wrapper.style.width * naturalHeight) / naturalWidth)) / 2,
                                    });
                                } else {
                                    this.$el.find('img').css({
                                        height: wrapper.style.height,
                                        width: (wrapper.style.height * naturalWidth) / naturalHeight,
                                        marginLeft: (wrapper.style.width - ((wrapper.style.height * naturalWidth) / naturalHeight)) / 2,
                                    });
                                }
                            } else if (this.model.get('settings').scaling == 'center') {
                                var src = this.model.get('settings').src,
                                    naturalWidth = $(wrapper).find('img')[0].naturalWidth || (function() {
                                        var img = new Image();
                                        img.src = src;
                                        return img.width;
                                    })(),
                                    naturalHeight = $(wrapper).find('img')[0].naturalHeight || (function() {
                                        var img = new Image();
                                        img.src = src;
                                        return img.height;
                                    })();

                                console.log(naturalWidth, naturalHeight);

                                if (wrapper.style.width > wrapper.style.height) {
                                    this.$el.find('img').css({
                                        height: wrapper.style.height,
                                        width: (wrapper.style.height * naturalWidth) / naturalHeight,
                                        marginLeft: (wrapper.style.width - ((wrapper.style.height * naturalWidth) / naturalHeight)) / 2,
                                    });
                                } else {
                                    this.$el.find('img').css({
                                        width: wrapper.style.width,
                                        height: (wrapper.style.width * naturalHeight) / naturalWidth,
                                        marginTop: (wrapper.style.height - ((wrapper.style.width * naturalHeight) / naturalWidth)) / 2,
                                    });
                                }
                            }
                        }
                        else {
                            //  If inner content suppress the wrapper height we should update height
                            var wrapperInnerHeight = 0;
                            $.each($(wrapper).find('> *'), function(index, element) {
                                wrapperInnerHeight += parseInt($(element).css('height'));
                            });
                            if (wrapperInnerHeight > parseInt(target.style.height)) {
                                target.style.height = wrapperInnerHeight + 'px';
                                wrapper.style.height = wrapperInnerHeight + 'px';
                                if (posy == 'top') {
                                    target.style.top = (parseFloat(target.style.top) - dy) + 'px';
                                    wrapper.style.top = (parseFloat(wrapper.style.top) - dy) + 'px';
                                }
                            }
                        }

                    }, this);

                    this.$el.find('.widget-resize-handle').each($.proxy(function(index, elm) {
                        Interact(elm)
                            .inertia({
                                resistance: 15,
                                zeroResumeDelta: true
                            })
                            .draggable({
                                axis: $(elm).attr('data-axis')
                            })
                            .on('dragmove', $.proxy(function(event) {
                                //	If shift has been hold it should resize with holding the structure
                                if (this.options.resizeable == 'xy' && event.shiftKey) {
                                    if ($(elm).attr('data-vertical-pos') == "midle") {
                                        event.dy = (this.el.offsetHeight * event.dx) / (this.el.offsetWidth);
                                        if ($(elm).attr('data-horizontal-pos') == 'right') {
                                            resize('right', 'bottom', event.dx, event.dy);
                                        } else {
                                            resize('left', 'bottom', event.dx, -event.dy);
                                        }

                                    } else if ($(elm).attr('data-horizontal-pos') == "center") {
                                        event.dx = (this.el.offsetWidth * event.dy) / (this.el.offsetHeight);
                                        if ($(elm).attr('data-vertical-pos') == 'top') {
                                            resize('right', 'top', -event.dx, event.dy);
                                        } else {
                                            resize('right', 'bottom', event.dx, event.dy);
                                        }
                                    }
                                    else {
                                        event.dx = (this.el.offsetWidth * event.dy) / (this.el.offsetHeight);
                                        if (($(elm).attr('data-vertical-pos') == 'top' && $(elm).attr('data-horizontal-pos') == 'left') || ($(elm).attr('data-vertical-pos') == 'bottom' && $(elm).attr('data-horizontal-pos') == 'right')) {
                                            resize($(elm).attr('data-horizontal-pos'), $(elm).attr('data-vertical-pos'), event.dx, event.dy);
                                        } else {
                                            resize($(elm).attr('data-horizontal-pos'), $(elm).attr('data-vertical-pos'), -event.dx, event.dy);
                                        }
                                    }
                                } else {
                                    resize($(elm).attr('data-horizontal-pos'), $(elm).attr('data-vertical-pos'), event.dx, event.dy);
                                }
                            }, this))
                            .on('dragend', dragend);
                    }, this));
                }
            }

            //	Setting styles for template
            this.styles['width'] = this.model.get('width') + 'px';
            this.styles['height'] = this.model.get('height') + 'px';
            this.styles['top'] = this.model.get('position_top') + 'px';
            this.styles['left'] = this.model.get('position_left') + 'px';
            this.styles['z-index'] = this.model.get('settings').z_index;

            //	Setting attributes
            this.attributes['data-x'] = this.model.get('position_left');
            this.attributes['data-y'] = this.model.get('position_top');

            //	Generate the context menu
            this.contextMenu = _.clone(this.options.contextMenu);
            this.contextMenu.push({
                id: 'addAnimation',
                target: 'animation',
                icon: 'fa fa-rocket',
                label: 'Add Animation'
            });
            this.contextMenu.push({
                id: 'showOnAllPages',
                func: this.showOnAllPages,
                icon: 'fa fa-check-circle',
                label: 'Show on All Pages',
                pre: this.model.get('show_on_all_pages') ? '<input type="checkbox" checked="checked">' : '<input type="checkbox">',
            });

            //	Default Context menu footer
            this.contextMenuFooter = new Array();
            this.contextMenuFooter.push({
                id: 'lock',
                func: this.lock,
                icon: 'fa fa-lock ' + (this.model.get('settings').lock ? 'locked' : ''),
                label: false
            });
            this.contextMenuFooter.push({
                id: 'copy',
                func: this.parent.copy,
                icon: 'fa fa-copy',
                label: false
            });
            this.contextMenuFooter.push({
                id: 'cut',
                func: this.parent.cut,
                icon: 'fa fa-cut',
                label: false
            });
            this.contextMenuFooter.push({
                id: 'forward',
                func: this.forward,
                icon: 'fa fa-level-up',
                label: false
            });
            this.contextMenuFooter.push({
                id: 'backward',
                func: this.backward,
                icon: 'fa fa-level-down',
                label: false
            });
            this.contextMenuFooter.push({
                id: 'delete',
                func: this.parent.eliminate,
                icon: 'fa fa-close',
                label: false
            });

            //	Constructing template
            this.$el.html(Template('widget', 'widget', {}));
            this.$el.find('div.wrapper')[0].innerHTML = this.options.contentView.render().el.outerHTML;

            //	Applying the styles
            this.$el.css(this.styles);

            //	Applying the attributes
            this.$el.attr(this.attributes);

            //	handle context menu events
            this.events['mouseleave ul.widget-context-menu'] = function(event) {
                //	When context menu unfocused, it should disappear
                this.$el.find('ul.widget-context-menu').fadeOut('2000');
            }

            //	Visual effects
            if (!this.model.get('settings').lock) {
                this.draggable();
                this.resizable();
            }

            this.events['contextmenu'] = this.events['click'] = function(event) {
                //	Stop browser default context menu
                event.preventDefault();

                //	Stop Event propagation in orther to prevent under layered widget context menu to appear
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }

                //	In order to divert focus form other context instances
                $(document).trigger('click');

                //	Context menu events
                var events = new Array();
                _.each(this.contextMenu.concat(this.contextMenuFooter), function(menu_item) {
                    events['click a#widget-context-menu-item-' + menu_item.id] = $.proxy(function(event) {
                        event.preventDefault();
                        //	In order to remove context menu
                        $(document).trigger('click');
                        if (menu_item.func && typeof(menu_item.func) === "function") {
                            //	if a function has been set, click event should trigger that function
                            $.proxy(menu_item.func, this)(event)
                        }
                    }, this);
                }, this);

                //	Create context layout
                var options = {
                    event: event,
                    events: events,
                    contextMenu: this.contextMenu,
                    contextMenuFooter: this.contextMenuFooter,
                }
                var contextMenuView = new ContextMenu(options);

                //	Show context menu
                contextMenuView.render();

                //  On event of click or right-click on any were beside context menu, shall close the menu
                $(document).one('click contextmenu scroll', function(event) {
                    contextMenuView.$el.remove();
                });

                //	Set time out to clear the context menu after a certian amount of time if mouse won't enter context menu
                var timeout = setTimeout($.proxy(function() {
                    contextMenuView.$el.remove();
                }, this), 2000);
                contextMenuView.$el.on('mouseenter', function() {
                    clearTimeout(timeout);
                });

                contextMenuView.$el.on('mouseleave', function(event) {
                    contextMenuView.$el.remove();
                });

                $('div.widget-base.focus').removeClass('focus');
                this.$el.addClass('focus');
            }

            this.delegateEvents();
            return this;
        },

        lock: function(event) {
            if (!this.model.get('settings').lock) {
                Interact(this.el).draggable(false).resizable(false);
                var settings = _.clone(this.model.get('settings'));
                settings.lock = true;
                this.model.set({
                    settings: settings
                }, {
                    validate: true
                });
            } else {
                var settings = _.clone(this.model.get('settings'));
                settings.lock = false;
                this.model.set({
                    settings: settings
                }, {
                    validate: true
                });
            }

        },

        showOnAllPages: function(event) {
            if (!this.model.get('show_on_all_pages')) {
                var showOnAllPagesEvent = document.createEvent('Event');
                showOnAllPagesEvent.initEvent('widgetShowOnAllPagesEnabled', true, true);
                showOnAllPagesEvent.instance = this.parent;
                document.dispatchEvent(showOnAllPagesEvent);
                this.model.set({
                    show_on_all_pages: true
                }, {
                    validate: true
                });
            } else {
                var showOnAllPagesEvent = document.createEvent('Event');
                showOnAllPagesEvent.initEvent('widgetShowOnAllPagesDisabled', true, true);
                showOnAllPagesEvent.instance = this.parent;
                document.dispatchEvent(showOnAllPagesEvent);
                this.model.set({
                    show_on_all_pages: false
                }, {
                    validate: true
                });
            }
        },

        forward: function(event) {
            var settings = _.clone(this.model.get('settings'));
            settings.z_index++;
            this.model.set({
                settings: settings
            }, {
                validate: true
            });
        },

        backward: function(event) {
            var settings = _.clone(this.model.get('settings'));
            if (settings.z_index - 1 > 0) {
                settings.z_index--;
                this.model.set({
                    settings: settings
                }, {
                    validate: true
                });
            }
        }
    });
})
