// app/src/components/editor/init.js

///
//
//	Editor Class
//		Creates and manages the whole editor app, load resources, creates views, load templates and so on
//		
//		@methods:
//			load(Void)					: Void			Generates a loading view, initiates server communications, loads resources and scripts to start application
//			getSite()					: Void			Gets site information from server and store it within editor instance
//			getPages(Void)				: Void 			Gets pages information from server and runs `initPages()` function
//			getWidgets(Void)			: Void 			Gets widgets information from server and runs `initWidgets()` function
//			initPages(JSON:pages)		: Void 			Prepares pages and page collection
//			initWidget(JSON:widgets)	: Void			Prepares widgets and store theme in widget collection 
//			renderPage(Integer:pageId)	: Object:HTML	Renders a page provided by `pageId`			
//			initEditor()				: Void			Generates editor view and combine it with home page rendered instance
//		@events:
//			pfin
//			ldfin
//			
//
///

define([
	//	Basic Dependencies
	'jquery',
	'underscore',
	'backbone',
	'notify',
	'mousetrap',
	'webfontloader',

	//	Local Dependencies
	'editor/views/editor',
	'editor/models/design',
	'editor/models/revision',
	'editor/models/theme',

	//	Methods
	'editor/src/initEditor',
	'editor/src/Factory',

	//	Components
	'templates',
	//'gallery',
	//'menu'
], function (
	//	Basic Dependencies
	$, _, Backbone, notify, Mousetrap, WebFontLoader,

	//	Local Dependencies
	EditorView, Design, Revision, Theme,

	//	Methods
	initEditor, Factory
	) {
	function Editor() {
		//	Components
		this.widgets = new Array(),
		this.pages = new Array();

		//	Factory is the class that creates instance of widgets for us
		this.Factory = Factory(this);

		//	Creates widget, attache it to the current page or all pages(if show on all page), chenges the page_id to current page id
		//	Rerenders current page, creates a History record
		this.paste = $.proxy(function (event) {
			if ( application.clipboard.length ) {
				console.log(event);
				//	Read widget data from clipboard
				var record = $.extend(true, {}, application.clipboard[application.clipboard.length-1]);

				if ( record.type === 'widget' ) {
					//	Update page_id of widget
					record.data.page_id = this.current_page.model.get('id');
					record.data.position_top = 100 + Math.floor((Math.random()*1000)%200);
					record.data.position_left = 100 + Math.floor((Math.random()*1000)%200);
					record.data.settings.parent = 'body';
					record.data.id = null;

					//	Create a new widget instance using the same data
					this.Factory[record.data.type](record.data);
				}
			}
		}, this);

		//	Undone revives an act from HistoryManagers redo or undo collection and undone it
		this.undone = function (direction) {
			var act = application.HistoryManager.pop(direction);
			if (act) {
				switch (act.action) {
					case "delete":
						this.Factory[act.data.type](act.data);
						break;
					case "create":
						act.data.eliminate();
						break;
					case "edit":
						console.log('under consideration!');
				}
			}
		}

		//	Editors Events and listeners
		this.events = {
			//	Loading events
			processFinished: document.createEvent('Event'),
			loadFinished: document.createEvent('Event'),

			//	Saving events
			widgetSave: document.createEvent('Event'),
			pageSave: document.createEvent('Event'),
			designSave: document.createEvent('Event'),

			//	Creation events
			pageCreate: document.createEvent('Event'),
			widgetCreate: document.createEvent('Event'),

			//	Change events
			pagesChange: document.createEvent('Event'),
			
			//	Editor events
			save: document.createEvent('Event'),
			copy: document.createEvent('Event'),
			cut: document.createEvent('Event'),
			eliminate: document.createEvent('Event'),
			paste: document.createEvent('Event'),
		}

		this.initiateEvents = function () {
			//	Loading events
			this.events.processFinished.initEvent('editorProcessFinished', true, true);
			this.events.loadFinished.initEvent('editorLoadFinished', true, true);

			//	Saving events
			this.events.widgetSave.initEvent('editorWidgetSave', true, true);
			this.events.pageSave.initEvent('editorPageSave', true, true);
			this.events.designSave.initEvent('editorDesignSave', true, true);

			//	Creation events
			this.events.pageCreate.initEvent('editorPageCreate', true, true);
			this.events.widgetCreate.initEvent('editorWidgetCreate', true, true);

			//	Change events
			this.events.pagesChange.initEvent('editorPagesChange', true, true);
			
			//	Editor events
			this.events.copy.initEvent('editorCopy', true, true);
			this.events.cut.initEvent('editorCut', true, true);
			this.events.eliminate.initEvent('editorDelete', true, true);
			this.events.paste.initEvent('editorPaste', true, true);
		}

		this.initiateListeners = function () {
			Mousetrap.bind(['ctrl+c', 'command+c'], $.proxy(function (event) {
				event.preventDefault();
				document.dispatchEvent(this.events.copy);
			}, this));
			Mousetrap.bind(['ctrl+s', 'command+s'], $.proxy(function (event) {
				event.preventDefault();
				this.save();
				$.notify("saved successfully!" ,{
					className: "success",
					position: "top left",
				});
			}, this));
			Mousetrap.bind(['ctrl+x', 'command+x'], $.proxy(function (event) {
				event.preventDefault();
				document.dispatchEvent(this.events.cut);
			}, this))
			Mousetrap.bind(['ctrl+v', 'command+v'], $.proxy(function (event) {
				event.preventDefault();
				this.paste(event);
			}, this))
			Mousetrap.bind(['ctrl+z', 'command+z'], $.proxy(function (event) {
				event.preventDefault();
				this.undone('undo');
			}, this))
			Mousetrap.bind(['ctrl+y', 'shift+command+z'], $.proxy(function (event) {
				event.preventDefault();
				this.undone('redo');
			}, this))
			Mousetrap.bind(['del'], $.proxy(function (event) {
				event.preventDefault();
				document.dispatchEvent(this.events.eliminate);
			}, this))

			//	Widget Delete Event is fired when event a widget is deleted
			//	This function should capture this event and remove the deleted widget instance form widget collection
			document.addEventListener('widgetDeleted', $.proxy(function (event) {
				//	Find deleted widget from widget collection and delete it
				_.each(this.widgets, function (widget, index) {
					if (event.instance === widget) {
						this.widgets.splice(index,1);
					}
				}, this);

				//	Find deleted widget from pages and delete it
				_.each(this.pages, function (page, page_index) {
					_.each(this.pages[page_index].widgets, function (widget, widget_index) {
						if (event.instance === widget) {
							this.pages[page_index].widgets.splice(widget_index,1);
						}
					}, this);
				}, this);

				this.view.render();

			}, this));

			//	Page Delete Event is fired when a page is deleted
			document.addEventListener('pageDeleted', $.proxy(function (event) {
				//	Find deleted page from page collection
				_.each(this.pages, function (page, index) {
					if (event.instance === page) {
						console.log(page);
						//	If deleted page is current page change page to home page
						if (this.current_page === event.instance) {
							//	If deleted page is home page change home page to the first page in list
							if (this.home_page === event.instance) {
								if (this.pages.length > 1) {
									this.home_page = _.find(this.pages, function (page) {
										return page != event.instance;
									});
								}
								else {
									this.home_page = this.Factory.page({
										title: 'Home Page',
										slug: 'home_page',
										is_homepage: true,
										settings: {
											content: JSON.parse(application.theme.get('template_htmls').without_sidebar)
										},
									}, new Array());
								}
							}
							this.current_page = this.home_page;
						}

						//	Delete widgets of page
						_.each(event.instance.widgets, function (widget) {
							widget.view.$el.remove();
							widget.model.trigger('destroy');
							_.each(this.widgets, function (widget, index) {
								if (event.instance === widget) {
									this.widgets.splice(index,1);
								}
							}, this);
						}, this);

						//	Delete page
						this.pages.splice(index,1);
					}
				}, this);
				
				this.view.page = this.current_page;
				this.view.render();

			}, this));

			//	When this event triggered, the current page should render again
			document.addEventListener('editorPageContainerRender', $.proxy(function (event) {
				this.current_page.view.render();
			}, this));

			//	Happens when a page was home page and is not now
			document.addEventListener('siteSetHomePage', $.proxy(function (event) {
				console.log('rank of event', event.rank);
				if (event.rank == 0) {
					var rank = 1;
				}
				else {
					var rank = 0;
				}
				console.log('selected rank', rank);
				var page = _.find(this.pages, function (page) {
					return page.model.get('settings').rank == rank;	
				});
				console.log('selected page', page);
				page.model.set('is_homepage', true);
				this.home_page = page;
			}, this));
		}
		///
		
		this.initiateListeners();
		this.initiateEvents();
		//	Public Methods
		this.load = function () {
			//	Initiating load process
			var finisedProcesses = 0;
			document.addEventListener('editorProcessFinished', $.proxy(function (event) {
				//	Happens when each of load processes finish, after all of processes has loaded successfully it will trigger `ldfin` event
				finisedProcesses++;
				if (finisedProcesses >= 6) {
					document.dispatchEvent(this.events.loadFinished);
				}
			}, this));

			document.addEventListener('editorLoadFinished', $.proxy(function (event) {
				//	Happens when all load processes has finished loading, after this, the control of application would be left to `startEditor()` method
				this.startEditor();
			}, this));

			this.getSite();
		}

		this.initEditor = $.proxy(initEditor, this),

		this.startEditor = function () {
			application.HistoryManager.clean();
			application.clipboard = [];
			$('body').html(this.view.render().$el);
			$(window).trigger('resize');
		}

		///

		//	Private Methods
		this.getSite = function () {
			//	This method gets site information, template, colors, fonts, defaults and so.
			$.ajax({
				context: this,
				url: application.URL.design_api,
				method: 'GET',
				success: function (data) {
					//	Construct design model
					application.design = new Design({
						background_settings: data.background_settings,
						color_settings: data.color_settings,
						font_settings: data.font_settings,
						styles: data.styles,
						id: data.id,
					});

					application.theme = new Theme({
						current_template: data.current_template,
						template_htmls: data.template_htmls,
						template_settings: data.template_settings,
						id: data.template_id,
					});

					document.dispatchEvent(this.events.processFinished);

					this.injectStyles();
					this.getWidgets();
					this.loadFonts();
				},
				error: function (err) {
					console.log('err: ', err);
				}
			});
		};

		this.colorStyles = function () {
			var style = $('<style id="colorStyle"></style>')
			_.each(application.design.toJSON().color_settings, function (color, className) {
				style.append("." + className + " { color: " + color + "!important }");
				style.append(".bg_" + className + " { background-color: " + color + "!important }")
				style.append(".border_" + className + " { border-color: " + color + "!important }")
			});
			style.append(".color_transparent{color:initial}.bg_color_transparent{background-color:transparent}.border_color_transparent{border-color:transparent}");
			$('head').find('style#colorStyle').remove();
			$('head').append(style);
		}

		this.fontStyles = function () {
			var style = $('<style id="fontStyle"></style>')
			_.each(application.design.toJSON().font_settings, function (options, className, index) {
				style.append(".font_" + className + " {" + _.map(options, function (value, key) {
					return key.replace('_', '-') + ":" + value
				}).join('!important;') + " }");
			});
			$('head').find('style#fontStyle').remove();
			$('head').append(style);
		}

		this.loadFonts = function () {

			WebFontLoader.load({
				google: {
					families: _.map(application.settings.fonts.googleFonts, function (font) {
						return font.loader;
					})
				},
				active: function () {
					console.log('Fonts loaded...');
				}
			})
		}

		this.templateStyles = function () {
			var style = $('<style id="tempateStyles"></style>')
			style.append(application.design.get('styles'));
			$('head').append(style);
		}

		this.backgroundStyles = function () {
			var style = $('<style id="backgroundStyles"></style>'),
				background_settings = application.design.get('background_settings');
			style.append("div.page-background-node.site-default-background {" + _.map(background_settings, function (value, key) {
				return key.replace('_', '-') + ":" + value
			}).join(';') + " }");
			$('head').find('style#backgroundStyles').remove();
			$('head').append(style);
		}

		this.injectStyles = function () {
			this.colorStyles();
			this.fontStyles();
			this.templateStyles();
			this.backgroundStyles();
			
			//	On change in color or font in design model, update and re-inject styles
			application.design.on('change:font_settings', function (event) {
				this.fontStyles();
			}, this);

			application.design.on('change:color_settings', function (event) {
				this.colorStyles();
			}, this);

			application.design.on('change:background_settings', function (event) {
				this.backgroundStyles();
			}, this);

			document.dispatchEvent(this.events.processFinished);
		}

		this.getPages = function () {
			//	This method gets pages and information about pages
			$.ajax({
				context: this,
				url: application.URL.page_api,
				method: 'GET',
				success: function (data) {
					_.each(data, function (page) {
						this.Factory.page(page, _.filter(this.widgets, function (widget) {
							return page.id == widget.model.get('page_id') || widget.model.get('show_on_all_pages');
						}, this));
					}, this);
					document.dispatchEvent(this.events.processFinished);
					this.initPages();
				}
			});
		};

		this.getWidgets = function () {
			//	This method gets widgets
			$.ajax({
				context: this,
				url: application.URL.widget_api,
				method: 'GET',
				success: function (data) {
					_.each(data, function (widget) {
						this.Factory[widget.type](widget);
					}, this);
					document.dispatchEvent(this.events.processFinished);
					this.getPages();
				}
			});
		};

		this.initPages = function () {
			this.home_page = _.find(this.pages, function (page) { return page.model.isHomePage() });
			if ( !this.home_page ) {
				this.home_page = this.Factory.page({
					title: 'Home Page',
					slug: 'home_page',
					is_homepage: true,
					settings: {
						content: JSON.parse(application.theme.get('template_htmls').without_sidebar)
					},
				}, new Array());
			}
			this.current_page = this.home_page;
			document.dispatchEvent(this.events.processFinished);
			this.initEditor();
		};
		///

		//	Starts save process
		this.save = function () {
			//	Listener listens to `saveFinished` events and when all the savable models
			//	(including widgets and pages) has saved successfully this will create the revision
			//	and finish the save process.
			var saved_widget_counter = 0,
				saved_page_counter = 0;

			$(document).on('designSaveFinished', $.proxy(function (event) {
				document.dispatchEvent(this.events.pageSave);
			}, this));

			$(document).on('pageSaveFinished', $.proxy(function (event) {
				saved_page_counter++;
				if (saved_page_counter === this.pages.length) {
					document.dispatchEvent(this.events.widgetSave);
				}
			}, this));

			$(document).on('widgetSaveFinished', $.proxy(function (event) {
				saved_widget_counter++;
				if (saved_widget_counter === this.widgets.length) {
					var new_revision = new Revision({
						design_id: application.design.get('id'),
						widgets: _.map(this.widgets, function (widget) { return widget.model.get('id')}),
						pages: _.map(this.pages, function (page) { return page.model.get('id')})		
					});
					console.log({
						design_id: application.design.get('id'),
						widgets: _.map(this.widgets, function (widget) { return widget.model.get('id')}),
						pages: _.map(this.pages, function (page) { return page.model.get('id')})		
					});
				}
			}, this));

			document.dispatchEvent(this.events.designSave);
		}
		///

		

		this.testWidgets = function () {
			this.load();
			window.test = this;
		}
	}

	return Editor;
});