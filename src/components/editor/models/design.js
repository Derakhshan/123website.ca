/**
 *
 * 		DESIGN MODEL
 * 		VERSION: 0.0.0
 *
 * 		DESCRIPTION: Contains all information about design of page including colors, fonts and background.
 *					 This model is saved within global name-space `application.design` 
 * 		
 */

define([
	'jquery',
	'underscore',
	'backbone',
], function ($, _, Backbone) {
	return Backbone.Model.extend({
		initialize: function () {
			//	Save Finished event initiate
			this.saveFinished.initEvent('designSaveFinished', true, true);
			//	We set listener of the model here
			$(document).on('editorDesignSave', $.proxy(function (event) {
				console.log('test');
				//	Check if object is new or has changed
				if (this.hasChanged() || this.isNew()) {
					this.save();
				}
				else {
					console.log('design no change');
					document.dispatchEvent(this.saveFinished);
				}
			}, this));
		},
		saveFinished: document.createEvent('Event'),
		save: function () {
			var data = {design: this.toJSON()};
			data.design.template_id = application.theme.get('id');
			data.design.template_settings = application.theme.get('template_settings');
			$.ajax({
				context: this,
				method: 'POST',
				url: application.URL.design_api,
				data: JSON.stringify(data),
				contentType: "application/json; charset=utf-8",
				dataType: 'json',
				success: function (data) {
					console.log(data);
					if (data.status === true) {
						this.set(data.design);
						document.dispatchEvent(this.saveFinished);
					}
					else {
						console.log('some error has happend!', data.errors);
					}
				},
				error: function () {
					window.setTimeout(this.save, 600);
				}
			});
		},
		defaults: {
			background_settings: {},
			color_settings: {},
			font_settings: {}	,
			id: 0,
		}
	});
});