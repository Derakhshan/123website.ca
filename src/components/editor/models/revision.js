/**
 *
 * 		REVISION MODEL
 * 		VERSION: 0.0.0
 *
 * 		DESCRIPTION: Revision model, which contains all methods and informations needed for work with revisions in editor
 * 		
 */

define([
	'jquery',
	'underscore',
	'backbone',
], function ($, _, Backbone) {
	return Backbone.Model.extend({
		initialize: function (options) {
			this.set('settings', options);
			this.save();
		},
		save: function () {
			$.ajax({
				context: this,
				method: 'POST',
				url: application.URL.revision_api,
				data: JSON.stringify({revision: this.toJSON()}),
				contentType: "application/json; charset=utf-8",
				dataType: 'json',
				success: function (data) {
					if (data.status === true) {
						this.set(data.revision);
					}
					else {
						console.log('some error has happend!', data.errors);
					}
				},
				error: function () {
					window.setTimeout(this.save, 600);
				}
			});
		},
		defaults: {
			published: false,
			important: false,
			is_current: true,
			settings: {
				design_id: 1,
				widgets: [],
				pages: []
			},
			preview_filename: ''
		}
	});
});