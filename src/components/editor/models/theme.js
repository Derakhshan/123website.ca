/**
 *
 * 		THEME MODEL
 * 		VERSION: 0.0.0
 *
 * 		DESCRIPTION: Contains all information about themes. It will be used by `Page` to construct pages. 
 *					 This model is saved within global name-space `application.theme` 
 * 		
 */

define([
	'jquery',
	'underscore',
	'backbone',
], function ($, _, Backbone) {
	return Backbone.Model.extend({
		defaults: {
			current_template: {},
			template_htmls: {},
			template_settings: 0,
			id: 0,
		}
	});
});