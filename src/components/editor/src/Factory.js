/**
 *		FACTORY METHOD
 *		VERSION: 0.0.1
 *
 *		@Class
 *			Editor
 *
 * 		This method is a factory for all components
 * 
 */

define([
	'jquery',
	'page/init',
	'text/init',
	'image/init',
	'menu/init',
	'line/init'
], function ($, Page, Text, Image, Menu, Line) {
	return function (context) {
		return {
			page: $.proxy(function (page, widget) {
				//	Create new instance of page
				var new_page = new Page(page, widget);

				//	Register page within the editor page collection
				this.pages.push(new_page);

				//	Register page within the editor view if view is created before
				if ( this.view ) {
					this.view.pages.push({
						index: this.view.pages.length,
						title: new_page.model.get('title')
					});

					//	Update editor current page refrence
					this.current_page = new_page;

					//	Update editor views current page refrence
					this.view.page = new_page;

					//	Remove selected status of current page from navigation
					this.view.$el.find('.editor-page-nav-selector option[selected="selected"]').removeAttr('selected');

					//	Set a new option for navigation
					this.view.$el.find('select.editor-page-nav-selector').append($('<option value="' + new_page.model.cid + '" selected="selected">'+ new_page.model.get('title') + '</option>'));

					//	Update page container
					this.view.$el.find('.editor-page-container').html(new_page.view.render().$el);
					
					//	Set navigation to the newly created page
					this.view.$el.find('.editor-page-nav-selector option[value="' + (this.view.pages.length-1) + '"]').attr('selected', 'selected');
				}
				
				document.dispatchEvent(this.events.pagesChange);

				return new_page;

			}, context),
			widget: $.proxy(function (new_widget) {
				//	Register within the editor widget collection
				this.widgets.push(new_widget);

				//	Attach widget to page or pages
				if ( this.pages.length && parseInt(new_widget.model.get('show_on_all_pages')) ) {
					//	If show on all pages is true, then attach widget to all pages
					_.each(this.pages, function (page) {
						page.widgets.push(new_widget)
					}, this);

					//	Re-render current page to see changes
					this.current_page.view.render();
				}
				else if ( this.pages.length ){
					//	If show on all page is not true, attach to the current_page
					this.current_page.widgets.push(new_widget);

					//	Re-render current page to see changes
					this.current_page.view.render();
				}

				//	Register history record
				application.HistoryManager.push({
					type: 'widget',
					action: 'create',
					data: new_widget
				});
			}, context),
			text: $.proxy(function (widget) {
				//	Create a new instance of widget
				var new_widget = new Text(widget);

				//	Call widget factory
				this.Factory.widget(new_widget);
			}, context),
			image: $.proxy(function (widget) {
				//	Create a new instance of widget
				var new_widget = new Image(widget);

				//	Call widget factory
				this.Factory.widget(new_widget);
			}, context),
			menu: $.proxy(function (widget) {
				//	Attach this.pages to menu model
				if ( widget && widget.settings ) {
					widget.settings.pages = this.pages;
				}
				else if (widget) {
					widget.settings = {
						pages: this.pages,
						parent: 'body',
						z_index: 100,
						lock: false,
					}
				}
				else {
					var widget = {
						settings: {
							pages: this.pages,
							parent: 'body',
							z_index: 100,
							lock: false,
						}
					}
				}
				
				//	Create a new instance of widget
				var new_widget = new Menu(widget);

				//	Call widget factory
				this.Factory.widget(new_widget);
			}, context),
			line: $.proxy(function (widget) {
				//	Create a new instance of widget
				var new_widget = new Line(widget);

				//	Call widget factory
				this.Factory.widget(new_widget);
			}, context),
		};
	}
});