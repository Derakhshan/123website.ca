/**
 *		INITEDITOR METHOD
 *		VERSION: 0.0.1
 *
 *		@Class
 *			Editor
 *
 * 		This method initiates editor and creates editor view
 * 
 */

define([
	'jquery',
	'color_selector',
	'file_manager',
	'editor/views/editor',
	'editor/views/pages.setting',
	'editor/views/design.setting',
	'editor/views/elements.setting',
	'editor/views/page.create',
	'editor/views/background.setting'
], function ($, ColorSelector, FileManager, EditorView, PagesSetting,
			DesignSetting, ElementsSetting, PageCreate, BackgroundSetting
	) {
	return function () {
		this.view = new EditorView({
			events: {
				//	Opens editor menu panel
				'click a.editor-control-menu-panel-link': function (event) {
					if (parseInt(this.$el.find('.editor-setting-panel').css('width')) == 0){
						this.$el.find('.editor-setting-panel').animate({width: '150px'});	
					}
					else {
						this.$el.find('.editor-setting-panel').animate({width: '0'});
					}
				},

				//	Renders and attaches pages.setting to editor menu panel
				'click #panel-close-btn': $.proxy(function (event) {						
					this.view.$el.find('.editor-setting-panel').hide();
					this.view.$el.find('.editor-control-menu li a').removeClass('current');
				}, this),

				//	Renders and attaches pages.setting to editor menu panel
				'click a#editor-pages-button': $.proxy(function (event) {
					var pages_settings = new PagesSetting({pages: this.pages});
					this.view.$el.find('.editor-setting-panel').html(pages_settings.render().$el).show();
				}, this),

				//	Renders and attaches editor.design view to editor menu panel
				'click a#editor-design-button': $.proxy(function (event) {
					var pages_settings = new DesignSetting();
					this.view.$el.find('.editor-setting-panel').html(pages_settings.render().$el).show();
				}, this),

				//	Renders and attaches editor.background.settings to the editor menu panel
				'click a#editor-background-button': $.proxy(function (event) {
					var background_setting = new BackgroundSetting();
					this.view.$el.find('.editor-setting-panel').html(background_settings.render().$el).show();
				}, this),

				//	Renders and attaches editor.add elements view to editor menu panel
				'click a#editor-add-elements-button': $.proxy(function (event) {
					var element_settings = new ElementsSetting();
					this.view.$el.find('.editor-setting-panel').html(element_settings.render().$el).show();
				}, this),
				'click a#editor-create-text-button': $.proxy(function (event) {
					this.Factory.text({page_id: this.current_page.model.get('id')});
				}, this),
				'click a#editor-create-image-button': $.proxy(function (event) {
					this.Factory.image({page_id: this.current_page.model.get('id')});
				}, this),
				'click a#editor-create-menu-button': $.proxy(function (event) {
					this.Factory.menu({page_id: this.current_page.model.get('id')});
				}, this),
				'click a#editor-create-line-button': $.proxy(function (event) {
					this.Factory.line({page_id: this.current_page.model.get('id')});
				}, this),
				'click a#editor-create-page-button': $.proxy(function (event) {
					//	Create a layout for page creation
					var page_create = new PageCreate();

					var ok = $.proxy(function () {
						if (page_create.$el.find('select.page-create-layout-select option:selected').val() != "none") {
							this.Factory.page({
								title: page_create.$el.find('.page-create-name').val(),
								slug: page_create.$el.find('.page-create-name').val().replace(/\s+/, "_"),
								settings: {
									content: JSON.parse(application.theme.get('template_htmls')[page_create.$el.find('select.page-create-layout-select option:selected').val()])
								},
							});
							page_create.render().$el.dialog('close');
						}
					}, this);

					//	Start dialog
					page_create.render().$el.dialog({
						title: "Add page",
						width: 400,
						height: 547,
						closeOnEscape: true,
						buttons: [{ 
							text: "Ok", 
							click: ok
						}],
						close: function (event) {
							$(this).dialog('destroy');
						}
					});
				}, this),
				'click a#editor-save-button': $.proxy(function (event) {
					this.save();
					$(event.target).notify(
						"saved successfully!",
						{
							position:"bottom",
							className: "success",
						}
					);
				}, this),
				'click a#editor-copy-button': $.proxy(function (event) {
					document.dispatchEvent(this.events.copy);
				}, this),
				'click a#editor-cut-button': $.proxy(function (event) {
					document.dispatchEvent(this.events.cut);
				}, this),
				'click a#editor-paste-button': $.proxy(function (event) {
					this.paste(event);
				}, this),
				'click a#editor-undo-button': $.proxy(function (event) {
					this.undone('undo');
				}, this),
				'change select.editor-page-nav-selector': $.proxy(function (event) {
					var selected = this.view.$el.find('.editor-page-nav-selector').val();

					_.each(this.pages, function (page, index) {
						if (selected == page.model.cid) {
							this.current_page = this.pages[index]; 
						}
					}, this);
					
					this.view.page = this.current_page;

					this.view.$el.find('.editor-page-container').html(this.current_page.view.render().$el)

					this.view.$el.find('.editor-page-nav-selector option[value="' + selected + '"]').attr('selected', 'selected');
				}, this),
				'click a#editor-preview-button': $.proxy(function (event) {
					if ($(event.target).attr('data-mode') == 'editor'){
						$('.page-zone-guide-line-vertical').css('display', 'none');
						$('.page-zone-guide-line-horizontal').css('display', 'none');
						$('.widget-controls').css('display', 'none');
						$('.page-zone-control').css('display', 'none');
						$('.editor-control-button-wrapper').css('display', 'none');
						$(event.target).html('Back To Edit');
						$(event.target).attr('data-mode', 'preview');
					}
					else {
						this.view.render();
					}
				}, this),
				'click a#editor-grid-button': $.proxy(function (event) {
					$('.page-zone-guide-line-vertical').toggle()
					$('.page-zone-guide-line-horizontal').toggle();
				}, this),
			},
			pages: this.pages,
			page: this.home_page,
		});
		document.dispatchEvent(this.events.processFinished);
	};
});