/**
 *
 *    PAGES SETTING VIEW
 *    VERSION: 0.0.0
 *
 *    Defines the setting view for pages
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'file_manager',
    'color_selector',
], function($, _, Backbone, Template, FileManager, ColorSelector) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'background-customize-wrapper',
        events: {
            'click a.page-setting-background-image-change': function(event) {
                FileManager('image', $.proxy(function(src) {
                    //  Change thumbnail
                    this.$el.find('.page-setting-background-image-thumbnail').css({
                        'background-image': "url(" + src + ")",
                        'background-size': 'cover',
                        'background-position': 'center center'
                    }).attr('data-src', src).attr('data-changed', 'true').removeClass('color_transparent');

                    //  Change style
                    $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-image: url(' + src + ')}');
                }, this));
            },
            'click a.page-setting-background-image-remove': function(event) {
                this.$el.find('.page-setting-background-image-thumbnail').css({
                    'background-image': "none",
                    'background-size': 'initial',
                    'background-position': 'initial'
                }).attr('data-src', 'none').attr('data-changed', 'true').addClass('color_transparent');

                //  Change style
                $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-image: none}');
            },
            'click span.page-setting-background-color-thumbnail': function(event) {
                ColorSelector('mini', $.proxy(function(color) {
                    this.$el.find('span.page-setting-background-color-thumbnail')[0].className = this.$el.find('span.page-setting-background-color-thumbnail')[0].className.replace(/ bg_.+[\s]|bg_.+$/, ' ');
                    this.$el.find('span.page-setting-background-color-thumbnail').removeClass('color_transparent').addClass('bg_' + color).attr('data-color', color).attr('data-changed', 'true');

                    //  Try out the color on the page background
                    $('.page-background-node')[0].className = $('.page-background-node')[0].className.replace(/ bg_.+[\s]|bg_.+$/, ' ');
                    $('.page-background-node.site-default-background-color').addClass('bg_' + color);
                }, this), event);
            },
            'click .background-positions li': function(event) {
                $('.background-positions li.current').removeClass('current');
                $(event.target).addClass('current');
                this.$el.find('input.image_position').val($(event.target).attr('data-value'));
            },
            'change input': function(event) {
                $(event.target).attr('data-changed', 'true');

                if ($(event.target).hasClass('image_scaling')) {
                    var background_image_scaling = this.$el.find('input.image_scaling:checked').val();
                    switch (background_image_scaling) {
                        case 'full_screen':
                            $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-size:cover;background-repeat:no-repeat}');
                            break;
                        case 'fit':
                            $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-size:contain;background-repeat:no-repeat}');
                            break;
                        case 'tile':
                            $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-size:auto;background-repeat:repeat}');
                            break;
                        case 'tile_vertically':
                            $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-size:auto;background-repeat:repeat-y}');
                            break;
                        case 'tile_horizontally':
                            $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-size:auto;background-repeat:repeat-x}');
                            break;
                        default:
                            $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-size:auto;background-repeat:no-repeat}');
                    }
                }
                // TODO: Ahmad jan lotfan in bakhsh ro taqir bede va event hasho set kon.
                else if ($(event.target).hasClass('image_position')) {
                    var background_image_position = this.$el.find('input.image_position:checked').val();alert()
                    switch (background_image_position) {
                        case 'top_center':
                            $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-position: 50% 0}');
                            break;
                        case 'top_right':
                            $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-position: 100% 0}');
                            break;
                        case 'middle_left':
                            $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-position: 0 50%}');
                            break;
                        case 'middle_center':
                            $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-position: 50% 50%}');
                            break;
                        case 'middle_right':
                            $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-position: 100% 50%}');
                            break;
                        case 'bottom_left':
                            $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-position: 0 100%}');
                            break;
                        case 'bottom_center':
                            $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-position: 50% 100%}');
                            break;
                        case 'bottom_right':
                            $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-position: 100% 100%}');
                            break;
                        default:
                            $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-position: 0 0}');
                    }
                }
                else if ($(event.target).hasClass('page-setting-background-attachment')) {
                    if ($(event.target).prop('checked')) {
                        $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-attachment:scroll}');
                    } else {
                        $('head').find('style#backgroundStyles').append('div.page-background-node.site-default-background{background-attachment:scroll}');
                    }
                }
            },
            'click a.background-customize-setting-save-button': function(event) {
                var background_setting = _.clone(this.model.get('background_settings'));

                if (this.$el.find('.page-setting-background-image-thumbnail').attr('data-changed') == 'true') {
                    var background_image_src = this.$el.find('.page-setting-background-image-thumbnail').attr('data-src');
                    if (background_image_src === 'none') {
                            background_setting.background_image = "none";
                    } else {
                            background_setting.background_image = "url(" + background_image_src + ")";
                    }
                }

                if (this.$el.find('input.image_scaling:checked').attr('data-changed') == 'true') {
                    var background_image_scaling = this.$el.find('input.image_scaling:checked').val();
                    switch (background_image_scaling) {
                        case 'full_screen':
                            background_setting.background_size = 'cover';
                            background_setting.background_repeat = 'no-repeat';
                            break;
                        case 'fit':
                            background_setting.background_size = 'contain';
                            background_setting.background_repeat = 'no-repeat';
                            break;
                        case 'tile':
                            background_setting.background_size = 'auto';
                            background_setting.background_repeat = 'repeat';
                            break;
                        case 'tile_vertically':
                            background_setting.background_size = 'auto';
                            background_setting.background_repeat = 'repeat-y';
                            break;
                        case 'tile_horizontally':
                            background_setting.background_size = 'auto';
                            background_setting.background_repeat = 'repeat-x';
                            break;
                        default:
                            background_setting.background_size = 'auto';
                            background_setting.background_repeat = 'no-repeat';
                            break;
                    }
                }

                if (this.$el.find('input.image_position:checked').attr('data-changed') == 'true') {
                    var background_image_position = this.$el.find('input.image_position:checked').val();
                    switch (background_image_position) {
                        case 'top_center':
                            background_setting.background_position = '50% 0';
                            break;
                        case 'top_right':
                            background_setting.background_position = '100% 0';
                            break;
                        case 'middle_left':
                            background_setting.background_position = '0 50%';
                            break;
                        case 'middle_center':
                            background_setting.background_position = '50% 50%';
                            break;
                        case 'middle_right':
                            background_setting.background_position = '100% 50%';
                            break;
                        case 'bottom_left':
                            background_setting.background_position = '0 100%';
                            break;
                        case 'bottom_center':
                            background_setting.background_position = '50% 100%';
                            break;
                        case 'bottom_right':
                            background_setting.background_position = '100% 100%';
                            break;
                        default:
                            background_setting.background_position = '0 0';
                    }
                }

                if (this.$el.find('input.page-setting-background-attachment').attr('data-changed') == 'true') {
                    if (this.$el.find('input.page-setting-background-attachment').prop('checked')) {
                        background_setting.background_attachment = 'scroll';
                    } else {
                        background_setting.background_attachment = 'fixed';
                    }
                }

                if (this.$el.find('span.page-setting-background-color-thumbnail').attr('data-changed') == 'true') {
                    var background_color = this.$el.find('span.page-setting-background-color-thumbnail').attr('data-color');
                    background_setting.background_color = background_color;
                }

                this.model.set('background_settings', background_setting);
            },
            'click a#panel-close-btn': function (event) {
                this.model.trigger('change:background_settings');
            },
            'click a.back-to-background-setting': function (event) {
                this.model.trigger('change:background_settings');
            }
        },
        initialize: function(options) {},
        render: function() {
            //  Constructing the template
            var background_setting = this.model.get('background_settings'),
                param = {
                    background_image: background_setting.background_image ? background_setting.background_image.substring(4, background_setting.background_image.length - 1) : false,
                    background_color: background_setting.background_color || 'color_transparent',
                }
            this.$el.html(Template('editor', 'background_customize', param));

            //  Chose current state of background scale based on element
            switch (background_setting.background_repeat) {
                case 'repeat-x':
                    this.$el.find('input.image_scaling[value=tile_horizontally]').attr('checked', 'checked');
                    break;
                case 'repeat-y':
                    this.$el.find('input.image_scaling[value=tile_vertically]').attr('checked', 'checked');
                    break;
                case 'repeat':
                    this.$el.find('input.image_scaling[value=tile]').attr('checked', 'checked');
                    break;
                default:
                    switch (background_setting.background_size) {
                        case 'cover':
                            this.$el.find('input.image_scaling[value=full_screen]').attr('checked', 'checked');
                            break;
                        case 'contain':
                            this.$el.find('input.image_scaling[value=fit]').attr('checked', 'checked');
                            break;
                        default:
                            this.$el.find('input.image_scaling[value=normal]').attr('checked', 'checked');
                    }
            }

            //  Chose current state of background position based on element
            switch (background_setting.background_position) {
                case "100% 100%":
                    this.$el.find('input.image_position[value=bottom_right]').attr('checked', 'checked');
                    break;
                case "50% 100%":
                    this.$el.find('input.image_position[value=bottom_center]').attr('checked', 'checked');
                    break;
                case "0px 100%":
                    this.$el.find('input.image_position[value=bottom_left]').attr('checked', 'checked');
                    break;
                case "100% 50%":
                    this.$el.find('input.image_position[value=middle_right]').attr('checked', 'checked');
                    break;
                case "50% 50%":
                    this.$el.find('input.image_position[value=middle_center]').attr('checked', 'checked');
                    break;
                case "0px 50%":
                    this.$el.find('input.image_position[value=middle_left]').attr('checked', 'checked');
                    break;
                case "100% 0px":
                    this.$el.find('input.image_position[value=top_right]').attr('checked', 'checked');
                    break;
                case "50% 0px":
                    this.$el.find('input.image_position[value=top_center]').attr('checked', 'checked');
                    break;
                default:
                    this.$el.find('input.image_position[value=top_left]').attr('checked', 'checked');
                    break;
            }

            //  Chose current state of background attachment based on element
            if (background_setting.background_attachment == "scroll") {
                this.$el.find('input.page-setting-background-attachment').attr('checked', 'checked');
            }


            return this;
        }
    });
});
