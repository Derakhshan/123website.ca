/**
 *
 *    WEBSITE BACKGROUND SETTING
 *    VERSION: 0.0.0
 *
 *    Defines the view to show preset backgrounds and customize button
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'editor/views/background.setting',
    'color_selector'
], function($, _, Backbone, Template, BackgroundSettings, ColorSetting) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'background-setting-wrapper',
        events: {
            'contextmenu': function(event) {
                event.preventDefault();
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
            },
            'click img.background-setting-preset-thumbnail': function(event) {
                //  Get preset object from data-id
                console.log('test');
                var preset = _.find(application.settings.site_dafault_backgrounds_preset, function(preset) {
                    return $(event.target).attr('data-id') == preset.id;
                });

                //  Update design background setting
                application.design.set({
                    background_settings: preset.styles
                }, {
                    validate: true
                });
            }
        },
        initialize: function(options) {},
        render: function() {
            //  Constructing the template
            this.$el.html(Template('editor', 'background_setting', {
                background_presets: application.settings.site_dafault_backgrounds_preset
            }));

            return this;
        }
    });
});
