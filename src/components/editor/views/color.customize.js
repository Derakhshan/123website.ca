/**
 *
 *    WEBSITE COLOR CUSTOMIZE
 *    VERSION: 0.0.0
 *
 *    Defines the view to customize colors
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'color_selector'
], function($, _, Backbone, Template, ColorPicker) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'color-customize-wrapper',
        events: {
            'contextmenu': function(event) {
                event.preventDefault();
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
            },
            'click a.color-customize-setting-save-button': function () {
                application.design.set('color_settings', this.picker.currentColors);
            },
            'click span#panel-close-btn, a.back-to-color-setting': function () {
                application.design.trigger('change:color_settings');
            }
        },
        initialize: function(options) {},
        render: function() {
            //  Constructing the template
            this.$el.html(Template('editor', 'color_customize', {
                color_presets: application.settings.site_dafault_color_preset
            }));

            //  Construct the picker
            this.picker = ColorPicker('full');
            this.$el.find('.setting-body').html(this.picker.render().$el);

            return this;
        }
    });
});
