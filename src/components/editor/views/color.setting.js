/**
 *
 *    WEBSITE COLOR SETTING
 *    VERSION: 0.0.0
 *
 *    Defines the view to show preset colors and customize button
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'color_selector'
], function($, _, Backbone, Template, ColorSetting) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'color-setting-wrapper',
        events: {
            'contextmenu': function(event) {
                event.preventDefault();
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
            },
            'click a.color-setting-preset': function(event) {
                //  Find the preset using the id on element
                var preset = _.find(application.settings.site_dafault_color_preset, function(preset) {
                    return $(event.currentTarget).attr('data-id') == preset.id;
                }),
                    mainColors = preset.colors.join(',');

                //  Get the palet from server! palet should be saved localy in feuther
                $.ajax({
                    context: this,
                    method: "GET",
                    url: application.URL.color_api,
                    data: {
                        mainColors: mainColors
                    },
                    success: function (data) {
                        application.design.set({
                            color_settings: data
                        }, {
                            validate: true
                        });
                    }
                });
            },
        },
        initialize: function(options) {},
        render: function() {
            //  Constructing the template
            this.$el.html(Template('editor', 'color_setting', {
                color_presets: application.settings.site_dafault_color_preset
            }));

            return this;
        }
    });
});
