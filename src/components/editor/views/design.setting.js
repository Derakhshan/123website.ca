/**
 *
 *    WEBSITE DESIGN SETTING
 *    VERSION: 0.0.0
 *
 *    Defines the setting view for website design
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'editor/views/background.setting',
    'editor/views/background.customize',
    'editor/views/color.setting',
    'editor/views/color.customize',
    'editor/views/font.setting',
    'editor/views/font.customize',
], function($, _, Backbone, Template, BackgroundSetting, BackgroundCustomize, ColorSetting, ColorCustomize, FontSetting, FontCustomize) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'design-setting-wrapper',
        events: {
            'contextmenu': function(event) {
                event.preventDefault();
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
            },
            'click a#design-setting-background-setting-btn,a.back-to-background-setting': function() {
                //  Create a background setting view
                var background_setting = new BackgroundSetting();
                this.$el.html(background_setting.render().$el);
            },
            'click a.background-setting-customize-button': function(event) {
                //  Create background customize view
                var background_customize = new BackgroundCustomize({
                    model: application.design
                });
                this.$el.html(background_customize.render().$el);
            },
            'click a#design-setting-color-setting-btn,a.back-to-color-setting': function() {
                //  Create color setting view
                var color_setting = new ColorSetting({
                    model: application.design
                });
                this.$el.html(color_setting.render().$el);
            },
            'click a.color-setting-customize-button': function (event) {
                //  Create color customize view
                var color_customize = new ColorCustomize({
                    model: application.design
                });
                this.$el.html(color_customize.render().$el);
            },
            'click a#design-setting-font-setting-btn,a.back-to-fonts': function (event) {
                //  Create font setting view
                var font_setting = new FontSetting({
                    model: application.design
                });
                this.$el.html(font_setting.render().$el);
            },
            'click a.font-setting-customize-button': function (event) {
                //  Create font setting view
                var font_customize = new FontCustomize({
                    model: application.design
                });
                this.$el.html(font_customize.render().$el);
            },
            'click a.back-to-design': function() {
                this.render();
            },
        },
        initialize: function(options) {},
        render: function() {
            //  Constructing the template
            this.$el.html(Template('editor', 'design_setting'));

            return this;
        }
    });
});
