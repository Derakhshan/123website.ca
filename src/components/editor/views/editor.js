/**
 *
 *      Editor MAIN VIEW
 *      VERSION: 0.0.0
 *
 *      The main section which ables user to create widgets, pages, save, publish, review, copy, paste, delete and so on
 *
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function($, _, Backbone, Template) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'editor-container',
        initialize: function(options) {
            this.pages = _.sortBy(options.pages, function(page) {
                return page.model.get('settings').rank
            });
            this.page = options.page;

            //  On every page change we need to re-render editor
            _.each(this.pages, function(page) {
                page.model.on('change', function(event) {
                    this.pages = _.sortBy(this.pages, function(page) {
                        return page.model.get('settings').rank
                    });
                    var param = _.map(this.pages, function(page, key) {
                        return {
                            index: page.model.cid,
                            title: page.model.get('title')
                        }
                    });
                    this.$el.find('.select-page').html(Template('editor', 'editor_page_select', {
                        pages: param
                    }));
                    this.$el.find('.select-page option[value=' + this.page.model.cid + ']').attr('selected', 'selected');
                }, this);
            }, this);

        },
        render: function() {
            //  Construct Editor template
            this.$el.html(Template('editor', 'editor', {}));

            //  Put page in place
            this.$el.find('.editor-page-container').html(this.page.view.render().$el);

            //  Put page selector in place
            var param = _.map(this.pages, function(page, key) {
                return {
                    index: page.model.cid,
                    title: page.model.get('title')
                }
            });
            this.$el.find('.select-page').html(Template('editor', 'editor_page_select', {
                pages: param
            }));
            this.$el.find('.select-page option[value=' + this.page.model.cid + ']').attr('selected', 'selected');

            this.$el.find('.toolbar-nav').click(function(){
                if(!$(this).hasClass('current')){
                    $('.toolbar-nav.current').removeClass('current');
                    $(this).addClass('current');
                }
            });

            return this;
        }
    });
});
