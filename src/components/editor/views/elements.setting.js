/**
 *
 *    WEBSITE DESIGN SETTING
 *    VERSION: 0.0.0
 *
 *    Defines the setting view for website design
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function($, _, Backbone, Template) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'elements-setting-wrapper',
        events: {},
        initialize: function(options) {},
        render: function() {
            //  Constructing the template
            this.$el.html(Template('editor', 'elements_setting'));

            return this;
        }
    });
});
