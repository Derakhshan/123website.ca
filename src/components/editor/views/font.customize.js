/**
 *
 *    WEBSITE FONT CUSTOMIZE
 *    VERSION: 0.0.0
 *
 *    Defines the view to customize fonts
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'editor/views/font.customize.selector'
], function($, _, Backbone, Template, FontSelector) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'font-customize-wrapper',
        events: {
            'contextmenu': function(event) {
                event.preventDefault();
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
            },
            // 'mouseover a.font-customize-preset': function (event) {
            //     $('head').find('style#fontStyle').append(
            //         ".font_" + $(event.currentTarget).attr('data-class-name').replace(' ', '_') + "{background-color:#fef9bf}"
            //     );
            // },
            // 'mouseleave a.font-customize-preset': function (event) {
            //     $('head').find('style#fontStyle').append(
            //         ".font_" + $(event.currentTarget).attr('data-class-name').replace(' ', '_') + "{background-color:transparent}"
            //     );
            // },
            'click a.font-customize-preset': function (event) {
                //  Lunch font selector
                var font_selector = new FontSelector({
                    className: $(event.currentTarget).attr('data-class-name').replace(' ', '_'),
                    font: this.current_font[$(event.currentTarget).attr('data-class-name').replace(' ', '_')],
                });

                var close = $.proxy(function (event) {
                    var style = $('head').find('style#fontStyle');
                    _.each(this.current_font, function (options, className, index) {
                        style.append(".font_" + className + " {" + _.map(options, function (value, key) {
                            return key.replace('_', '-') + ":" + value
                        }).join('!important;') + " }");
                    });
                    font_selector.$el.dialog('destroy');
                }, this);

                var save = $.proxy(function () {
                    this.current_font[$(event.currentTarget).attr('data-class-name').replace(' ', '_')] = {
                        font_family: font_selector.$el.find('select#font_family').val(),
                        font_size: font_selector.$el.find('input#font_size').val(),
                        font_weight: font_selector.$el.find('input#font_weight').prop('checked')?"bold":"normal",
                        font_style: font_selector.$el.find('input#font_style').prop('checked')?"italic":"normal",
                    }
                    font_selector.$el.dialog('close');
                }, this);

                font_selector.render().$el.dialog({
                    title: $(event.currentTarget).attr('data-class-name'),
                    width: 400,
                    closeOnEscape: true,
                    buttons: [{ 
                        text: "Save",
                        click: save
                    }],
                    close: close
                });
            },
            'click a.font-customize-save-button': function (event) {
                application.design.set('font_settings', this.current_font);
            },
            'click span#panel-close-btn,a.back-to-fonts': function (event) {
                application.design.trigger('change:font_settings');
            }
        },
        initialize: function(options) {
            this.current_font = $.extend(true, {}, application.design.get('font_settings'));
        },
        render: function() {
            //  Constructing the template
            var font = _.map(this.current_font, function (font, className) {
                return {
                    name: className.replace('_', ' '),
                    className: "font_" + className,
                }
            });
            this.$el.html(Template('editor', 'font_customize', {
                font: font
            }));

            return this;
        }
    });
});
