/**
 *
 *    WEBSITE FONT SELECTOR
 *    VERSION: 0.0.0
 *
 *    Defines the view to font selector dialog
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
], function($, _, Backbone, Template) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'font-selector-wrapper',
        events: {
            'contextmenu': function(event) {
                event.preventDefault();
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
            },
            'change select.font-selector-font-family-select': function (event) {
                //  Get select value
                var font_family = $(event.target).val(),
                    style = $('head').find('style#fontStyle');

                style.append(".font_" + this.targetClassName + " {font-family:" + font_family +"!important;}");
            },
            'change input#font_size': function (event) {
                var font_size = $(event.target).val(),
                    style = $('head').find('style#fontStyle');

                style.append(".font_" + this.targetClassName + " {font-size:" + font_size +"!important;}");
            },
            'change input#font_weight': function (event) {
                var style = $('head').find('style#fontStyle');
                
                if ($(event.target).prop('checked')) {
                    style.append(".font_" + this.targetClassName + " {font-weight:bold!important;}");
                }
                else {
                    style.append(".font_" + this.targetClassName + " {font-weight:normal!important;}");
                }
            },
            'change input#font_style': function (event) {
                var style = $('head').find('style#fontStyle');
                console.log($(event.target).prop('checked'), event.target);
                if ($(event.target).prop('checked')) {
                    style.append(".font_" + this.targetClassName + " {font-style:italic!important;}");
                }
                else {
                    style.append(".font_" + this.targetClassName + " {font-style:normal!important;}");
                }
            },
        },
        initialize: function(options) {
            this.targetClassName = options.className;
            this.font = options.font;
        },
        render: function() {
            //  Constructing the template
            this.$el.html(Template('editor', 'font_customize_selector', {
                fonts: application.settings.fonts.systemFonts.concat(application.settings.fonts.googleFonts),
            }));

            //  Select the current font in select box
            var option = _.find(this.$el.find('option.font-selector-font-family'), function (elm) {
                return elm.value.split(',')[0].trim() == this.font.font_family.split(',')[0].trim();
            }, this);
            $(option).attr('selected', 'selected');

            //  Set initial values of inputs
            this.$el.find('input#font_size').val(this.font.font_size);

            if (this.font.font_weight == 'bold') {
                this.$el.find('input#font_weight').attr('checked', 'checked');
            }

            if (this.font.font_style == 'italic') {
                this.$el.find('input#font_style').attr('checked', 'checked');
            }

            return this;
        }
    });
});
