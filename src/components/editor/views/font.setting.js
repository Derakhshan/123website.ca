/**
 *
 *    WEBSITE FONT SETTING
 *    VERSION: 0.0.0
 *
 *    Defines the view to show preset fonts and customize button
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
], function($, _, Backbone, Template) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'font-setting-wrapper',
        events: {
            'contextmenu': function(event) {
                event.preventDefault();
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
            },
            'click a.font-setting-preset': function(event) {
                //  Find the preset using the id on element
                var preset = _.find(application.settings.site_dafault_font_preset, function(preset) {
                    return $(event.currentTarget).attr('data-id') == preset.id;
                });

                //  Update design model
                this.model.set('font_settings', preset.fonts);
            },
        },
        initialize: function(options) {},
        render: function() {
            //  Constructing the template
            this.$el.html(Template('editor', 'font_setting', {
                font_presets: application.settings.site_dafault_font_preset
            }));

            return this;
        }
    });
});
