//	app/src/components/editor/views/load.js

///
//
//	Load View
//		A minimal view that will be shown while the application is loading.
//		@dependencies:
//			[Backbone:[jquery,underscore], templates]
//		@methods:
//			None.
//		@functionality:
//			None.
//		@options:
//			None.
//	
///

define([
	'jquery',
	'underscore',
	'backbone',
	'templates',
], function ($, _, Backbone, Template) {
	return Backbone.View.extend({
		tagName: 'div',
		className: 'editor-loading',
		initialize: function () {
		},
		render: function () {
			this.$el.html(Template('editor', 'load', {}));
			return this;
		}
	});
});