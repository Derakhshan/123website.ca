/**
 *
 * 		PAGE CREATE
 * 		VERSION: 0.0.0
 *
 *		Defines the view for page create dialog
 */

define([
  'jquery',
  'underscore',
  'backbone',
  'templates',
], function ($, _, Backbone, Template, BackgroundSettings, ColorSetting) {
  return Backbone.View.extend({
    tagName: 'div',
    className: 'page-create-wrapper',
    events: {
      'change select.page-create-layout-select': function (event) {
        if ($('select.page-create-layout-select option:selected').val() != "none") {
          $('.page-create-layout-thumbnail').removeClass('color_transparent').attr('src', $('select.page-create-layout-select option:selected').attr('data-thumbnails'));
        }
        else {
         $('.page-create-layout-thumbnail').removeAttr('src').addClass('color_transparent');
        }
      }
    },
    initialize: function (options) {
    },
    render : function () {
      //  Constructing the template
      this.$el.html(Template('editor', 'create_page', {
        templates: _.map(application.theme.get('template_settings'), function (path, name) {
          return {
            name: name,
            thumbnail: "/editor/assets/images/tmb_1.png",
          }
        })
      }));

      $('select.page-create-layout-select').trigger('change');

      return this;
    }
  });
});