/**
 *
 *    PAGES SETTING VIEW
 *    VERSION: 0.0.0
 *
 *    Defines the setting view for pages
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'jquery-ui/sortable'
], function($, _, Backbone, Template, sortable) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'pages-setting-wrapper',
        events: {
            'contextmenu': function(event) {
                event.preventDefault();
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
            },
            "click a.pages-setting-page-setting-btn": function(event) {
                //  Find out which page's setting is invoced
                var page;
                if ($(event.currentTarget).attr('data-page-id')) {
                    page = _.find(this.pages, function(page) {
                        return page.model.get('id') == $(event.currentTarget).attr('data-page-id');
                    }, this);
                } else {
                    page = _.find(this.pages, function(page) {
                        return page.model.cid == $(event.currentTarget).attr('data-page-cid');
                    }, this);
                }

                page.setting();

            },
            "click li.pages-setting-page-item": function(event) {
                $('option[value="' + $(event.currentTarget).attr('data-page-cid') + '"]').attr('selected', 'selected');
                $('select.editor-page-nav-selector').trigger('change');
            }
        },
        initialize: function(options) {
            this.pages = options.pages;

            //  On editor pages change event, re-render the view
            $(document).on('editorPagesChange', $.proxy(function() {
                this.render();
            }, this));

            //  On change of any of pages re-render the view
            _.each(this.pages, function(page) {
                page.model.on('change', function() {
                    this.render();
                }, this)
            }, this);
        },
        render: function() {
            //  Constructing the template
            var param = {
                pages: _.map(_.sortBy(this.pages, function(page) {
                    return page.model.get('settings').rank
                }), function(page) {
                    return {
                        title: page.model.get('title'),
                        id: page.model.get('id'),
                        cid: page.model.cid
                    }
                }),
            }

            this.$el.html(Template('editor', 'pages_setting', param));

            //  Page order sortable
            this.$el.find(".pages-setting-pages-list-container").sortable({
                axis: "y",
                opacity: 0.5,
                revert: true,
                handle: ".pages-setting-page-order-btn",
                placeholder: "pages-setting-page-item-placeholder",
                deactivate: $.proxy(function(event, ui) {
                    this.$el.find(".pages-setting-pages-list-container li.pages-setting-page-item").each($.proxy(function(index, elm) {
                        var page = _.find(this.pages, function(page) {
                            return page.model.cid == $(elm).attr('data-page-cid')
                        }, this);
                        var settings = _.clone(page.model.get('settings'));
                        settings.rank = index;
                        page.model.set('settings', settings);
                    }, this));
                }, this)
            });
            this.$el.find(".pages-setting-pages-list-container").disableSelection();

            return this;
        }
    });
});
