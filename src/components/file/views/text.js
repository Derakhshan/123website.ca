define([
	'jquery',
	'underscore',
	'backbone',
	'templates',
	'interact',
	'modal'
], function ($, _, Backbone, Template, Interact) {
	return Backbone.View.extend({
		tagName: 'div',
		className: 'text-widget-wraper',
		initialize: function () {
			this.$el.html("<h1>Heading 1</h1><br ><h2>Heading 2</h2><br ><h3>Heading 3</h3><br ><h4>Heading 4</h4><br ><h5>Heading 5</h5><br ><h6>Heading 6</h6><br ><small>small</small><br ><p>Paragraph</p>")
		}
	});
});