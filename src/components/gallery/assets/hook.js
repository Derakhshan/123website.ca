define([
	'jquery',
	'inject',
	path.gallery + 'assets/javascript/tide'
], function ($, Inject, tide) {
	return function (gallery) {
		Inject(path.gallery + 'assets/stylesheet/grid');
		console
		var imageList = $(gallery).find('.gallery-wrapper ul');
		tide(imageList);
		$(gallery).find('.pagination .left').on('clikc', function () {
			console.log('left');
			imageList.find('li.active').removeClass('active').next().addClass('active');
			$(gallery).find('.gallery-wrapper .preview').html(imageList.find('li.active img').clone());
			$(gallery).find('.caption').html(imageList.find('li.active span').html());
		});
		$(gallery).find('.pagination .right').on('clikc', function () {
			console.log('right');
			imageList.find('li.active').removeClass('active').prev().addClass('active');
			$(gallery).find('.gallery-wrapper .preview').html(imageList.find('li.active img').clone());
			$(gallery).find('.caption').html(imageList.find('li.active span').html());
		});
	}
});