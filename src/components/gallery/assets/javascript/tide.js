define(['jquery'], function ($) {
	return function (list) {
		$(list).css('list-style', 'none');
		$.each($(list).find('li'), function (index, item) {
			$(item).css('float', 'left').css('padding-right', '10px');
		});
	};
});