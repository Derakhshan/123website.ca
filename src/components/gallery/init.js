
define([
	'jquery',
	'underscore',
	'backbone',
	'templates',
	'widget',
	//'src/components/gallery/views/gallery'
], function ($, _, Backbone, Templates, Widget, View) {
	return Widget.extend({
		contentView: new View(),
		width: 750,
	});
});