define([
	'jquery',
	'underscore',
	'backbone',
	'templates',
	path.gallery + 'assets/hook'
], function ($, _, Backbone, Template, Hook) {
	return Backbone.View.extend({
		tagName: 'div',
		className: 'gallery-widget-wraper',
		initialize: function () {
			Template('/app/src/components/gallery/templates/gallery', this, this.render);
		},
		render: function (template, root) {
			root.$el.html(template);
			Hook(root.$el);
		}
	});
});