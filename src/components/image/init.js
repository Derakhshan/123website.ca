/**
 *		IMAGE WIDGET
 *		VERSION: 0.0.1
 *
 * 		This widget manges all single image contents in editor.
 *
 * 		@dependencies:
 * 			Widget 			Based on base-widget class
 *			File Manager 	Uses file manager to change image
 *			Aviary			Uses Aviary(tm) as a photo minipulation tool
 * 		@views
 * 			Image 	A Backbone View, manages the main content of widget
 * 			Setting A Backbone View, manages the main settings of text widget
 * 			Change 	A Backbone View, manages upload, image management and so
 * 			Edit 	A Backbone View, contains image editing tools [not implemented in this version]
 * 			Link 	A Backbone View, ables Image to link into anything
 * 			Style 	A Backbone View, gives special styles to images
 * 		@models
 * 			Main 	A Backbone model, containing main features of text widget
 *
 */

define([
    'jquery',
    'underscore',
    'widget',
    'file_manager',
    'interact',
    'jquery-ui/dialog',
    'image/views/image',
    'image/models/image',
    'image/views/setting',
], function($, _, Widget, FileManager, Interact, Dialog, Image, Model, Setting) {
    var editImage = function(event) {
        var lunchEditor = function(uid, model) {
                //	Check if image editor app is ready
                if (!application.flags.image_editor) {
                    setTimeout(function() {
                        lunchEditor(uid, model)
                    }, 3000);
                } else {
                    application.app.ImageEditor.launch({
                        image: uid,
                        url: model.get('settings').src,
                        onSave: function(imageID, newURL) {
                            $.ajax({
                                url: application.URL.aviary_save_api,
                                method: 'post',
                                data: {
                                    url: newURL,
                                    file_title: model.get('settings').title
                                },
                                success: function(data) {
                                    var settings = _.clone(model.get('settings'));
                                    settings.src = data.file.src;
                                    settings.title = data.file.title;
                                    model.set({
                                        settings: settings
                                    }, {
                                        validate: true
                                    });
                                }
                            });
                        },
                        onClose: function(isDirty) {
                            $("#" + uid).remove();
                        }
                    });
                }
            }
            //	Create editable shadow element of image
        var shadow = this.$el.find("div.image-widget-wraper img").clone(),
            uid = _.uniqueId('image_editor_');
        $(shadow).attr('id', uid);
        $(shadow).css('display', 'none');

        //	Append shadow element to the body of document
        $('body').append(shadow);

        lunchEditor(uid, this.model);
    };

    var changeImage = function(event) {
        FileManager('image', $.proxy(function(file, title) {
            var settings = _.clone(this.model.get('settings'));
            settings.src = file;
            settings.title = title;
            this.model.set({
                settings: settings
            }, {
                validate: true
            });
        }, this));
    };

    return Widget.extend({
        contentFactory: Image,
        modelFactory: Model,
        resizeable: 'xy',
        events: {
            'dblclick': changeImage
        },
        contextMenu: [{
                id: 'change',
                func: function(event) {
                    this.$el.trigger('dblclick');
                },
                icon: 'fa fa-edit',
                label: 'Change Image'
            }, {
                id: 'edit',
                func: editImage,
                icon: 'fa fa-edit',
                label: 'Edit Image'
            },
            // {
            // 	id: 'link',
            // 	func: function (event, options) {
            // 		editMode(options);
            // 	},
            // 	icon: 'fa fa-edit',
            // 	label: 'Link to'
            // },
            {
                id: 'setting',
                func: function() {
                    //	Freez the widget
                    Interact(this.el).draggable(false);
                    this.undelegateEvents();

                    var save = $.proxy(function() {
                        this.model.set({
                            position_left: layout.$el.find('#left').val(),
                            position_top: layout.$el.find('#top').val(),
                            width: layout.$el.find('#width').val(),
                            height: layout.$el.find('#height').val(),
                            show_on_all_pages: layout.$el.find('#show_on_all_pages').prop('checked')
                        }, {
                            validate: true
                        });
                        layout.$el.dialog('close');
                    }, this);

                    var layout = new Setting({
                        model: this.model,
                    });

                    layout.events['click .image-setting-change-image-btn'] = $.proxy(function(event) {
                        $.proxy(changeImage, this)(event);
                    }, this);

                    layout.events['click .image-setting-edit-image-btn'] = $.proxy(function(event) {
                        $.proxy(editImage, this)(event);
                    }, this);

                    layout.events['change #left'] = $.proxy(function(event) {
                        this.$el.css('left', layout.$el.find('#left').val() + "px");
                    }, this);

                    layout.events['change #top'] = $.proxy(function(event) {
                        this.$el.css('top', layout.$el.find('#top').val() + "px");
                    }, this);

                    layout.events['change #width'] = $.proxy(function(event) {
                        this.$el.css('width', layout.$el.find('#width').val() + "px");
                    }, this);

                    layout.events['change #height'] = $.proxy(function(event) {
                        this.$el.css('height', layout.$el.find('#height').val() + "px");
                    }, this);

                    layout.render().$el.dialog({
                        title: "Image Settings",
                        width: 350,
                        height: 566,
                        closeOnEscape: true,
                        buttons: [{
                            text: "Save",
                            click: save
                        }],
                        close: $.proxy(function(event) {
                            this.render();
                            layout.$el.dialog('destroy');
                        }, this)
                    });
                },
                icon: 'fa fa-cog',
                label: 'Settings'
            }
            // {
            // 	id: 'style',
            // 	func: function (event, options) {
            // 		editMode(options);
            // 	},
            // 	icon: 'fa fa-edit',
            // 	label: 'Change Style'
            // }
        ],
    });
});
