/**
 *
 * 		IMAGE WIDGET CHANGE MODAL
 * 		VERSION: 0.0.0
 *
 *		Defines the setting model for text widget
 */

define([
  'jquery',
  'underscore',
  'backbone',
  'templates',
], function ($, _, Backbone, Template) {
  return Backbone.View.extend({
    tagName: 'div',
    className: 'modal-wrapper',
    initialize: function (options) {
      param = {
        options: options,
        _this: this,
        width: options.model.get('size')[0],
        height: options.model.get('size')[1],
        x: options.model.get('position')[1],
        y: options.model.get('position')[0],
        sap: options.model.get('showOnAllPages')
      }
      Template(window.ROOT + path.text + 'templates/setting', param, this.render);
    },
    render : function (template, param) {
      param._this.$el.html(template);
      param._this.$el.find('#edit').on('click', function (event) {
        param.options.editMode(param.options);
        param._this.$el.find('.modal').modal('hide');
      });
      param._this.$el.find('.modal').on('hidden.bs.modal', function (e) {
        console.log('test');
        param.options.model.set('position', [param._this.$el.find('#y').val(), param._this.$el.find('#x').val()]);
        param.options.model.set('size', [param._this.$el.find('#width').val(), param._this.$el.find('#height').val()]);
        param.options.model.set('showOnAllPages', param._this.$el.find('#showOnAllPages').prop('checked'));
        param.options.root.reRender(param.options);
      });
    }
  });
});