define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
], function($, _, Backbone, Template) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'image-widget-wrapper',
        events: {},
        initialize: function() {
        },
        render: function() {
            //	Costruct the template
            this.$el.html(Template('image', 'image', {
                src: this.model.get('settings').src
            }));

            //	Scale image
            this.$el.css({
                width: this.model.get('width'),
                height: this.model.get('height'),
            });
            if (this.model.get('settings').scaling == 'stretch') {
                this.$el.find('img').css({
                    width: "100%",
                    height: "100%"
                });
            } else if (this.model.get('settings').scaling == 'crop') {
                var src = this.model.get('settings').src,
                    naturalWidth = this.$el.find('img')[0].naturalWidth || (function() {
                        var img = new Image();
                        img.src = src;
                        return img.width;
                    })(),
                    naturalHeight = this.$el.find('img')[0].naturalHeight || (function() {
                        var img = new Image();
                        img.src = src;
                        return img.height;
                    })();

                console.log(naturalWidth, naturalHeight);

                if (this.model.get('width') > this.model.get('height')) {
                    this.$el.find('img').css({
                        width: this.model.get('width'),
                        height: (this.model.get('width') * naturalHeight) / naturalWidth,
                        marginTop: (this.model.get('height') - ((this.model.get('width') * naturalHeight) / naturalWidth)) / 2,
                    });
                } else {
                    this.$el.find('img').css({
                        height: this.model.get('height'),
                        width: (this.model.get('height') * naturalWidth) / naturalHeight,
                        marginLeft: (this.model.get('width') - ((this.model.get('height') * naturalWidth) / naturalHeight)) / 2,
                    });
                }
            } else if (this.model.get('settings').scaling == 'center') {
                var src = this.model.get('settings').src,
                    naturalWidth = this.$el.find('img')[0].naturalWidth || (function() {
                        var img = new Image();
                        img.src = src;
                        return img.width;
                    })(),
                    naturalHeight = this.$el.find('img')[0].naturalHeight || (function() {
                        var img = new Image();
                        img.src = src;
                        return img.height;
                    })();

                console.log(naturalWidth, naturalHeight);

                if (this.model.get('width') > this.model.get('height')) {
                    this.$el.find('img').css({
                        height: this.model.get('height'),
                        width: (this.model.get('height') * naturalWidth) / naturalHeight,
                        marginLeft: (this.model.get('width') - ((this.model.get('height') * naturalWidth) / naturalHeight)) / 2,
                    });
                } else {
                    this.$el.find('img').css({
                        width: this.model.get('width'),
                        height: (this.model.get('width') * naturalHeight) / naturalWidth,
                        marginTop: (this.model.get('height') - ((this.model.get('width') * naturalHeight) / naturalWidth)) / 2,
                    });
                }
            }

            return this;
        }
    });
});
