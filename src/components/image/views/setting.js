/**
 *
 *    IMAGE WIDGET SETTING MODAL
 *    VERSION: 0.0.0
 *
 *    Defines the setting model for image widget
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
], function($, _, Backbone, Template) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'image-setting-wrapper',
        events: {
            'contextmenu': function(event) {
                event.preventDefault();
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
            },
        },
        initialize: function() {

        },
        render: function() {
            //  Constructing the template
            this.$el.html(Template('image', 'setting', this.model.toJSON()));

            this.delegateEvents();
            return this;
        }
    });
});
