/**
 *		LINE WIDGET
 *		VERSION: 0.0.1
 *
 * 		This widget manges line widget
 *
 * 		@dependencies:
 * 			Widget 	Based on base-widget class
 * 		@views
 * 			Text 	A Backbone View, manages the main content of widget
 * 			Setting A Backbone View, manages the main settings of text widget
 * 		@models
 * 			Main 	A Backbone model, containing main features of text widget 
 * 
 */

define([
	'jquery',
	'underscore',
	'widget',
	'line/models/line',
	'line/views/line',
], function (jQuery, _,  Widget, Model, View) {	
	return Widget.extend({
		contentFactory: View,
		modelFactory: Model,
		resizeable: 'x',
		events: {
			'dblclick': function() {}
		},
		modals: {
			settings: {
				id: 'setting',
				layout: function() {}
			}
		}
	});
});