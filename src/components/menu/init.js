/**
 *		MENU WIDGET
 *		VERSION: 0.0.1
 *
 * 		This widget manges menu like widgets.
 *
 * 		@dependencies:
 * 			Widget 	Based on base-widget class
 * 		@views
 * 			Menu 	A Backbone View, manages the main content of menu
 * 			Setting A Backbone View, manages the main settings of text widget
 * 		@models
 * 			Main 	A Backbone model, containing main features of text widget
 *
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'widget',
    'menu/views/menu',
    'menu/models/menu',
    'menu/views/setting'
], function($, _, Backbone, Templates, Widget, Menu, Model, Setting) {
    return Widget.extend({
        contentFactory: Menu,
        modelFactory: Model,
        resizeable: 'xy',
        events: {
            'click a.menu-widget-menu-link': function(event) {
                event.preventDefault();
            }
        },
        contextMenu: [{
            id: 'navigate',
            func: function(event) {
                $('a#editor-pages-button').trigger('click');
            },
            icon: 'fa fa-location-arrow',
            label: 'Navigate'
        }],
        modals: {
            settings: {
                id: 'setting',
                layout: Setting
            }
        }
    });
});
