/**
 *
 * 		MENU MODEL
 * 		VERSION: 0.0.0
 *
 * 		Defines the model for menu widgets, contains default values, validation check, server communication functionality,
 * 		setting change functionality and so on.
 * 		
 */

define([
	'jquery',
	'underscore',
	'backbone',
], function ($, _, Backbone) {
	return Backbone.Model.extend({
		initialize: function () {
			//	Private Save method
			var save = $.proxy(function () {
				//	Check if object is new or has changed
				if (this.hasChanged() || this.isNew()) {
					//	We have to remove pages instance from menu before save
					var widget = this.toJSON()
					delete widget.settings.pages;

					$.ajax({
						context: this,
						method: 'POST',
						url: application.URL.widget_api,
						data: JSON.stringify({widget: widget}),
						contentType: "application/json; charset=utf-8",
						dataType: 'json',
						success: function (data) {
							if (data.status === true) {
								this.set(data.widget);
								document.dispatchEvent(this.saveFinished);
							}
							else {
								console.log('some error has happend!', data.errors);
							}
						},
						error: function () {
							window.setTimeout(this.save, 600);
						}
					});	
				}
				else {
					console.log('widget no change', this);
					document.dispatchEvent(this.saveFinished);
				}	
			}, this);

			//	Save Finished event initiate
			this.saveFinished.initEvent('widgetSaveFinished', true, true);

			//	We set listener of the model here
			document.addEventListener('editorWidgetSave', save);

			//	Upon the model destroy event all listeners shall be unhooked
			this.on('destroy', function (event) {
				document.removeEventListener('editorWidgetSave', save);
			}, this);

			//	Upon change on pages model
		},
		toJSON: function () {
			return $.extend(true, {}, this.attributes);
		},
		validate: function (attrs, options) {
			console.log('test');
		},
		saveFinished: document.createEvent('Event'),
		defaults: {
			width: 350,
			height: 40,
			position_top: 200,
			position_left: 400,
			page_id: 0,
			type: 'menu',
			show_on_all_pages: false,
			settings: {
				parent: 'body',
				z_index: 100,
				lock: false,
				alignment: "left",
			}
		}
	});
});