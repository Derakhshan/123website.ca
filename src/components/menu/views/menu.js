/**
 *
 * 		MENU VIEW
 * 		VERSION: 0.0.0
 *
 *		Defines the main view for menu widget
 */
define([
	'jquery',
	'underscore',
	'backbone',
	'templates',
], function ($, _, Backbone, Template) {
	return Backbone.View.extend({
		tagName: 'div',
		className: 'menu-widget-wrapper',
		events: {
			'click menu-widget-menu-link': function (event) {
				event.preventDefault();
			}
		},
		initialize: function () {
			$(document).on('editorPagesChange', $.proxy(function (event) {
				this.render();

				//	Set static content of view in model
				var settings = _.clone(this.model.get('settings'));
				settings.content = this.$el.html();
				this.model.set('settings', settings);

			}, this));

			//	On model change render the view again
			this.model.on('change', function () {
				this.render();
			}, this);

			//  On every page change we need to re-render editor
            _.each(this.model.get('settings').pages, function(page) {
                page.model.on('change', function(event) {
                    this.render();
                }, this);
            }, this);
		},
		render: function () {
			//	Constructing the view
			var pages = _.sortBy(this.model.get('settings').pages, function(page) {
                return page.model.get('settings').rank
            }),
			param = {
				pages: _.map(pages, function (page) {
					return {
						title: page.model.get('title'),
						cid: page.model.cid,
						href: "/dummylink"
					}
				})
			};
			this.$el.html(Template('menu', 'menu', param));

			//	Set default styles
			this.$el.css({
				width: this.model.get('width') + "px",
				height: this.model.get('height') + "px",
				'z-index': this.model.get('settings').z_index,
				'text-align': this.model.get('settings').alignment,
			});

			return this;	
		}
	});
});