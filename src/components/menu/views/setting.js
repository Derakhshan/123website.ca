/**
 *
 * 		MENU WIDGET SETTING MODAL
 * 		VERSION: 0.0.0
 *
 *		Defines the setting model for text widget
 */

define([
  'jquery',
  'underscore',
  'backbone',
  'templates',
], function ($, _, Backbone, Template) {
  return Backbone.View.extend({
    tagName: 'div',
    className: 'modal-wrapper',
    events: {
      "dialogbeforeclose": function () {
        var settings = _.clone(this.model.get('settings'));
        settings.alignment = this.$el.find('#alignment').val();
        this.model.set({
          position_left: this.$el.find('#left').val(),
          position_top: this.$el.find('#top').val(),
          width: this.$el.find('#width').val(),
          height: this.$el.find('#height').val(),
          show_on_all_pages: this.$el.find('#show_on_all_pages').prop('checked'),
          settings: settings
        }, {validate: true});
      }
    },
    initialize: function () {
      
    },
    render : function () {
      //  Constructing the template
      this.$el.html(Template('menu', 'setting', this.model.toJSON()));
      return this;
    }
  });
});