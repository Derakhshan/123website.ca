/**
 *		PAGES
 *		VERSION: 0.0.1
 *
 * 		This class handels page layout and model
 *
 */

define([
    'page/views/page',
    'page/views/setting',
    'page/models/page',
    'file_manager',
    'color_selector',
], function(View, Setting, Model, FileManager, ColorSelector) {
    return function(page, widgets) {
        this.model = new Model(page);
        this.widgets = widgets || new Array();
        this.view = new View({
            model: this.model,
            widgets: this.widgets
        });

        this.setting = function() {
            var setting = new Setting({
                model: this.model,
                parent: this
            }),
            originalSettings = _.clone(this.model.toJSON()),
            saved = false;

            setting.events['click a.page-setting-background-image-change'] = $.proxy(function () {
                FileManager('image', $.proxy(function(src) {
                    //  Change thumbnail
                    setting.$el.find('.page-setting-background-image-thumbnail').css({
                        'background-image': "url(" + src + ")",
                        'background-size': 'cover',
                        'background-position': 'center center'
                    }).attr('data-src', src).attr('data-changed', 'true').removeClass('color_transparent');

                    //  Chnage Background
                    this.view.$el.find('.page-background-node').css({
                        'background-image': "url(" + src + ")",
                    });
                }, this));
            }, this);

            setting.events['click a.page-setting-background-image-remove'] = $.proxy(function(event) {
                //  Change thumbnails
                setting.$el.find('.page-setting-background-image-thumbnail').css({
                    'background-image': "none",
                    'background-size': 'initial',
                    'background-position': 'initial'
                }).attr('data-src', 'none').attr('data-changed', 'true').addClass('color_transparent');

                //  Change background
                this.view.$el.find('.page-background-node').css({
                    'background-image': "none",
                });
            }, this);

            setting.events['change input.image_scaling'] = $.proxy(function(event) {
                var background_image_scaling = setting.$el.find('input.image_scaling:checked').val();
                switch (background_image_scaling) {
                    case 'full_screen':
                        this.view.$el.find('.page-background-node').css({
                            'background-size': 'cover',
                            'background-repeat': 'no-repeat'
                        });
                        break;
                    case 'fit':
                        this.view.$el.find('.page-background-node').css({
                            'background-size': 'contain',
                            'background-repeat': 'no-repeat'
                        });
                        break;
                    case 'tile':
                        this.view.$el.find('.page-background-node').css({
                            'background-size': 'auto',
                            'background-repeat': 'repeat'
                        });
                        break;
                    case 'tile_vertically':
                        this.view.$el.find('.page-background-node').css({
                            'background-size': 'auto',
                            'background-repeat': 'repeat-y'
                        });
                        break;
                    case 'tile_horizontally':
                        this.view.$el.find('.page-background-node').css({
                            'background-size': 'auto',
                            'background-repeat': 'repeat-x'
                        });
                        break;
                    default:
                        this.view.$el.find('.page-background-node').css({
                            'background-size': 'auto',
                            'background-repeat': 'no-repeat'
                        });
                        break;
                }
            }, this);

            setting.events['change input.image_position'] = $.proxy(function(event) {
                var background_image_position = setting.$el.find('input.image_position:checked').val();
                switch (background_image_position) {
                    case 'top_center':
                        this.view.$el.find('.page-background-node').css({
                            'background-position': '50% 0',
                        });
                        break;
                    case 'top_right':
                        this.view.$el.find('.page-background-node').css({
                            'background-position': '100% 0',
                        });
                        break;
                    case 'middle_left':
                        this.view.$el.find('.page-background-node').css({
                            'background-position': '0 50%',
                        });
                        break;
                    case 'middle_center':
                        this.view.$el.find('.page-background-node').css({
                            'background-position': '50% 50%',
                        });
                        break;
                    case 'middle_right':
                        this.view.$el.find('.page-background-node').css({
                            'background-position': '100% 50%',
                        });
                        break;
                    case 'bottom_left':
                        this.view.$el.find('.page-background-node').css({
                            'background-position': '0 100%',
                        });
                        break;
                    case 'bottom_center':
                        this.view.$el.find('.page-background-node').css({
                            'background-position': '50% 100%',
                        });
                        break;
                    case 'bottom_right':
                        this.view.$el.find('.page-background-node').css({
                            'background-position': '100% 100%',
                        });
                        break;
                    default:
                        this.view.$el.find('.page-background-node').css({
                            'background-position': '0 0',
                        });
                        break;
                }
            }, this);

            setting.events['change input.page-setting-background-attachment'] = $.proxy(function(event) {
                if (setting.$el.find('input.page-setting-background-attachment').prop('checked')) {
                    this.view.$el.find('.page-background-node').css({
                        'background-attachment': 'scroll',
                    });
                } else {
                    this.view.$el.find('.page-background-node').css({
                        'background-attachment': 'fixed',
                    });
                }
            }, this);

            setting.events['click span.page-setting-background-color-thumbnail'] = $.proxy(function(event) {
                ColorSelector('mini', $.proxy(function(color) {
                    //  Set thumbnails
                    setting.$el.find('span.page-setting-background-color-thumbnail')[0].className = setting.$el.find('span.page-setting-background-color-thumbnail')[0].className.replace(/ bg_.+[\s]|bg_.+$/, ' ');
                    setting.$el.find('span.page-setting-background-color-thumbnail').removeClass('color_transparent').addClass('bg_' + color).attr('data-color', color).attr('data-changed', 'true');

                    //  Set background color
                    this.view.$el.find('.page-background-node')[0].className = this.view.$el.find('.page-background-node')[0].className.replace(/ bg_.+[\s]|bg_.+$/, ' ');
                    this.view.$el.find('.page-background-node').addClass('bg_' + color);
                    this.view.$el.find('.page-background-node').removeClass('site-default-background-color');
                }, this), event);
            }, this);

            var save = $.proxy(function(event) {
                //	This command is essential for changes to effect
                this.view.update();
                saved = true;
                
                console.log(this.model.hasChanged());
                setting.$el.dialog('close');
            }, this);

            setting.render().$el.dialog({
                title: this.model.get('title') + " Settings",
                width: 350,
                height: 566,
                closeOnEscape: true,
                buttons: [{
                    text: "Save",
                    click: save
                }],
                close: $.proxy(function(event) {
                    if (!saved) {
                        this.model.set(originalSettings);    
                    }
                    
                    //  If this is home page (whether it changed or not)
                    if (this.model.get('is_homepage')) {
                        var home_page_change_event = document.createEvent('Event');
                        home_page_change_event.initEvent('siteHomePageChange', true, true);
                        home_page_change_event.cid = this.model.cid;
                        document.dispatchEvent(home_page_change_event);
                    }

                    this.view.render();
                    setting.$el.dialog('destroy');
                }, this)
            })
        }
    }
});
