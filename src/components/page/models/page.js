/**
 *
 * 		PAGE MODEL
 * 		VERSION: 0.0.0
 *
 * 		The main model for page, contains the main attributes of a page
 * 		
 */

define([
	'underscore',
	'backbone',
], function (_, Backbone) {
	return Backbone.Model.extend({
		initialize: function (options) {
			//	If content is not provided (new template) populate with bare html markup
			if ( !this.get('settings').content ) {
				this.set('settings', {
					content: JSON.parse(application.theme.get('template_htmls').with_two_sidebar)
				});
			}

			//	Private Save method
			var save = $.proxy(function (event) {
				//	Check if object is new or has changed
				if (this.hasChanged() || this.isNew()) {
					$.ajax({
						context: this,
						method: 'POST',
						url: application.URL.page_api,
						data: JSON.stringify({page: this.toJSON()}),
						contentType: "application/json; charset=utf-8",
						dataType: 'json',
						success: function (data) {
							if (data.status === true) {
								this.set(data.page);
							}
							else {
								console.log('some error has happend!', data.errors);
							}
						},
						error: function () {
							window.setTimeout(this.save, 600);
						}
					});
				}
				else {
					console.log('page no change');
					document.dispatchEvent(this.saveFinished);
				}
			}, this);

			//	Save Finished event initiate
			this.saveFinished.initEvent('pageSaveFinished', true, true);

			//	We set listener of the model here
			document.addEventListener('editorPageSave', save);

			//	If home page is about to change, all pages shall be checked
			document.addEventListener('siteHomePageChange', $.proxy(function (event) {
				if (this.get('is_homepage')) {
					if (event.cid != this.cid) {
						this.set('is_homepage', false);
					}
				}
			}, this));

			//	Upon the model destroy event all listeners shall be unhooked
			this.on('destroy', function (event) {
				document.removeEventListener('editorPageSave', save);
			}, this);
		},
		isHomePage: function () {
			return this.get('is_homepage') == true;
		},
		isNew: function () {
			if (!this.has('id')) {
				return true;
			}
			else if (/^page_/.test(this.get('id'))) {
				return true;
			}
			return false;
		},
		saveFinished: document.createEvent('Event'),
		defaults: {
			id: _.uniqueId('page_'),
			title: 'New Page',
			slug: 'New_Page',
			is_homepage: false,
			hide_from_menu: false,
			settings: {
				content: '',
				rank: 100,
			},
			meta_title: "",
			meta_description: "",
			meta_keywords: ""
		}
	});
});