/**
 *
 *    PAGE CONTEXT MENU VIEW
 *    VERSION: 0.0.0
 *
 *    Defines the view page's context menu
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'page/views/zone.style',
], function($, _, Backbone, Template, Styles) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'zone-context-wrapper',
        events: {
            'contextmenu': function(event) {
                //  Prevent context menu propagation
                event.preventDefault();
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
            },
            'click a#zone-context-style-settings': function(event) {
                var save = $.proxy(function(event) {
                    $(this.target).trigger('change');
                    styles.$el.dialog('close');
                }, this);

                var styles = new Styles({
                    target: this.target
                });
                styles.render().$el.dialog({
                    title: this.target.tagName + ' styles',
                    width: 350,
                    height: 566,
                    closeOnEscape: true,
                    close: $.proxy(function(event) {
                        styles.$el.dialog('destroy');
                        var pageContainerRender = document.createEvent('Event');
                        pageContainerRender.initEvent('editorPageContainerRender', true, true);
                        document.dispatchEvent(pageContainerRender);
                    }, this),
                    buttons: [{
                        text: "Save",
                        click: save
                    }],
                });
            }
        },
        initialize: function(options) {
            this.styles = {
                top: event.clientY + "px",
                left: event.clientX + "px",
                position: 'fixed',
                z_index: 1500,
            };
            this.target = options.event.currentTarget;

            this.render();
        },
        render: function() {
            //  Constructing the template
            this.$el.html(Template('page', 'context', {
                zone_name: this.target.tagName.charAt(0).toUpperCase() + this.target.tagName.slice(1).toLowerCase()
            }));

            //  Set position of the context menu
            this.$el.css(this.styles);

            //  Attach this to the body
            $('body').append(this.$el);

            //  On event of click or right-click on any were beside context menu, shall close the menu
            $(document).one('click contextmenu scroll', $.proxy(function(event) {
                this.$el.remove();
            }, this));

            //  Set time out to clear the context menu after a certian amount of time if mouse won't enter context menu
            var timeout = setTimeout($.proxy(function() {
                this.$el.remove();
            }, this), 2000);
            this.$el.on('mouseenter', function() {
                clearTimeout(timeout);
            });

            this.$el.on('mouseleave', $.proxy(function(event) {
                this.$el.remove();
            }, this));

            return this;
        }
    });
});
