/**
 *
 * 		Page VIEW
 * 		VERSION: 0.0.0
 *
 *		Defines the main view for text widgets
 */


define([
	'jquery',
	'underscore',
	'backbone',
	'templates',
	'interact',
	'page/views/context',
	'page/views/zone.control',
], function ($, _, Backbone, Template, Interact, Context, Control) {
	return Backbone.View.extend({
		tagName: 'div',
		className: 'page-wrapper',
		events: {
		},
		update: function () {
			//	Remove all the widgets to create a bare structure
			_.each(this.widgets, function (widget) {
				widget.view.$el.remove();
			});

			//	Remove all the zone-control from dom
			this.$el.find('.page-zone-control').each(function (index, elm) {
				elm.remove();
			});

			//	Save the structure with in the model
			var settings = _.clone(this.model.get('settings'));
			settings.content = this.$el.find('div#editorThemeBody').html().trim();
			console.log(settings);
			this.model.set({
				settings: settings
			}, {validate: true})

			//	Re-render the page
			this.render();
		},
		initialize: function (options) {
			//	This event triggers editor current page render
			this.pageContainerRender = document.createEvent('Event');
			this.pageContainerRender.initEvent('editorPageContainerRender', true, true);

			//	This event is dispatched when a page or pages has changes
			this.pagesChange = document.createEvent('Event');
			this.pagesChange.initEvent('editorPagesChange', true, true);

			//	On event of save, page id might change, which will lead us to update all widgets `page_id`
			this.model.on('change:id', function (event) {
				_.each(this.widgets, function (widget) {
					widget.model.set('page_id', this.model.get('id'));
				}, this);
				document.dispatchEvent(this.model.saveFinished);
			}, this);

			//	On event of model change, re-render the page view
			// this.model.on('change', function (event) {
			// 	document.dispatchEvent(this.pagesChange);
			// 	this.render();
			// }, this);

			//	When a widget goes in show on all page mode, every page should attach this widget to their widget
			//	collection. in start all widgets with show on all pages option will be distribute with editor to every page
			document.addEventListener('widgetShowOnAllPagesEnabled', $.proxy(function (event) {
				if ( !_.find(this.widgets, function (widget) { return widget === event.instance}, this) ) {
					this.widgets.push(event.instance);
				}
			}, this));

			//	When show on all pages disabled, the widget should be removed from all pages but the main page
			//	which is the page with id same as widgets page_id
			document.addEventListener('widgetShowOnAllPagesDisabled', $.proxy(function (event) {
				if (this.model.get('id') != event.instance.model.get('page_id')) {
					_.each(this.widgets, function (widget, index) {
						if (event.instance === widget) {
							this.widgets.splice(index, 1);
						}
					}, this);
				}
				document.dispatchEvent(this.pageContainerRender);
			}, this));

			//	If background_setting has changes and this pages is using default background
			//	settings then it must update it self(but only in case of background_color change)
			application.design.on('change:background_settings', function (event) {
				if (this.$el.find('.page-background-node').hasClass('site-default-background-color')) {
					this.render();
				}
			}, this);

			this.widgets = options.widgets;
			this.render();
		},
		render: function () {
			//	Place the primary backbone of page
			this.$el.html(Template('page', 'page', {theme: this.model.get('settings').content}));

			//	Place every widget in place
			_.each(this.widgets, function (widget) {
				if (widget.model.get('settings').parent == 'body' ) {
					this.$el.find('.site-structure').append(widget.render().$el);
				}
				else {
					this.$el.find(widget.model.get('settings').parent).append(widget.render().$el);
				}
			}, this);

			//	Default background set
			if (this.$el.find('.page-background-node').hasClass('site-default-background-color')) {
				this.$el.find('.page-background-node')[0].className = this.$el.find('.page-background-node')[0].className.replace(/ bg_.+[\s]|bg_.+$/, ' ');
				this.$el.find('.page-background-node').addClass('bg_'+application.design.get('background_settings').background_color);
			}

			//	Create Zones
			this.$el.find('.page-zone').each(function(index, elm) {
				$(elm).append(new Control({parent: elm}).render().$el);
			});
			this.$el.find('.page-zone').on('change', $.proxy(function (event) {
				this.update();	
			}, this));

			var resizeEnd = $.proxy(this.update, this);
			var drop = $.proxy(function (event) {
				//	Transfer widget into the zone
		    	$(event.target).append(event.relatedTarget);
			}, this);
			
			Interact('.page-zone')
			.dropzone({
			    // listen for drop related events:
			    accept: '.widget-base',
			    ondropactivate: function (event) {
			        // add active dropzone feedback
			        event.target.classList.add('drop-active');
			    },
			    ondragenter: function (event) {
			        event.target.classList.add('drop-target');
			    },
			    ondragleave: function (event) {
			        // remove the drop feedback style
			        event.target.classList.remove('drop-target');
			        event.relatedTarget.classList.remove('can-drop');
			    },
			    ondrop: drop,
			    ondropdeactivate: function (event) {
			        // remove active dropzone feedback
			        event.target.classList.remove('drop-active');
			        event.target.classList.remove('drop-target');
			    }
			}) 
			.inertia(true);

			//	Context menu for zones
			this.$el.find('.page-zone').on('contextmenu click', $.proxy(function (event) {
				event.preventDefault();
		        event.cancelBubble = true;
		        if (event.stopPropagation) {
		           event.stopPropagation(); 
		        }

		        //	To order to divert focus
		        $(document).trigger('click');

		        new Context({model: this.model, event: event});

			}, this));

			//	Auto-width and Auto-height nodes
			this.$el.find('.auto-width-node').css({
				width: window.innerWidth,
			});
			this.$el.find('.auto-height-node').css({
				height: window.innerHeight,
			});
			$(window).on('resize', $.proxy(function (event) {
				this.$el.find('.auto-width-node').css({
					width: window.innerWidth,
				});
				this.$el.find('.auto-height-node').css({
					height: window.innerHeight,
				});
			}, this));

			//	fix-width and fix-height nodes
			$(window).on('resize', $.proxy(function (event) {
				this.$el.find('.fix-width-node').each(function (index, elm) {
					$(elm).css({
						width: $('body')[0].offsetWidth + "px",
						left: "-" + $(elm).parents('.page-zone')[0].offsetLeft + "px" 
					});
				});
				this.$el.find('.fix-height-node').each(function (index, elm) {
					$(elm).css({
						height: $('body')[0].offsetHeight + "px",
						top: "-" + $(elm).parents('.page-zone')[0].offsetTop + "px" 
					});
				});
			}, this));

			$(window).trigger('resize');

			return this;
		}
	});
});