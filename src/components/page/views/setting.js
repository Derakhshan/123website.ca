/**
 *
 *    PAGE SETTING VIEW
 *    VERSION: 0.0.0
 *
 *    Defines the setting view for pages
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'file_manager',
    'color_selector',
    'jquery-ui/dialog'
], function($, _, Backbone, Template, FileManager, ColorSelector, Dialog) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'page-setting-wrapper',
        events: {
            'click a.page-setting-page-delete': function(event) {
                if (confirm('Are you sure about deleting this page?')) {
                    //  Register action in history
                    application.HistoryManager.push({
                        type: 'page',
                        action: 'delete',
                        data: {
                            page: this.model.toJSON(),
                            widgets: _.map(this.parent.widgets, function(widget) {
                                return widget.model.toJSON()
                            })
                        }
                    });

                    //  remove dialog
                    this.$el.dialog('close');

                    //  remove element
                    this.parent.view.$el.remove()

                    //  destroy model
                    this.parent.model.trigger('destroy');

                    //  dispatch page delete event
                    var pageDelete = document.createEvent('Event');
                    pageDelete.initEvent('pageDeleted', true, true);
                    pageDelete.instance = this.parent;

                    document.dispatchEvent(pageDelete, true, true);
                }
            },
            'change input': function(event) {
                $(event.target).attr('data-changed', 'true');
            },
            'change input#title': function(event) {
                this.model.set('title', this.$el.find('input#title').val());
            },
            'change input#slug': function(event) {
                this.model.set('slug', this.$el.find('input#slug').val());
            },
            'change input#is_homepage': function(event) {
                //  If this page is marked as home page other pages must not be home page
                if (this.$el.find('input#is_homepage').prop('checked')) {
                    var home_page_change_event = document.createEvent('Event');
                    home_page_change_event.initEvent('siteHomePageChange', true, true);
                    home_page_change_event.cid = this.model.cid;
                    document.dispatchEvent(home_page_change_event);
                } else {
                    //  If this page was home page is not now
                    var set_home_page_event = document.createEvent('Event');
                    set_home_page_event.initEvent('siteSetHomePage', true, true);
                    set_home_page_event.rank = this.model.get('settings').rank;
                    document.dispatchEvent(set_home_page_event);
                }
                console.log(this.model.get('settings'));
                this.model.set('is_homepage', this.$el.find('input#is_homepage').prop('checked'));
            },
            'change input#hide_from_menu': function(event) {
                this.model.set('hide_from_menu', this.$el.find('input#hide_from_menu').prop('checked'));
            },
            'change input#meta_title': function(event) {
                this.model.set('meta_title', this.$el.find('input#meta_title').val());
            },
            'change input#meta_description': function(event) {
                this.model.set('meta_description', this.$el.find('input#meta_description').val());
            },
            'change input#meta_keywords': function(event) {
                this.model.set('meta_keywords', this.$el.find('input#meta_keywords').val());
            },
        },
        initialize: function(options) {
            this.parent = options.parent;
        },
        render: function() {
            //  Constructing the template
            var param = this.model.toJSON(),
                node = $(this.model.get('settings').content).find('.page-background-node')[0];

            //  Extract background Image from page background node
            if (node.style.backgroundImage) {
                param.background_image = node.style.backgroundImage.substring(4, node.style.backgroundImage.length - 1);
            } else {
                param.background_image = false;
            }

            //  Extract background color from page background node
            _.each(node.className.split(/\s+/), function(className) {
                if (/^bg_.+$/.test(className)) {
                    param.background_color = className;
                }
            }, this);
            if (!param.background_color) {
                param.background_color = "color_transparent";
            }

            this.$el.html(Template('page', 'setting', param));

            //  Chose current state of background scale based on element
            switch (node.style.backgroundRepeat) {
                case 'repeat-x':
                    this.$el.find('input.image_scaling[value=tile_horizontally]').attr('checked', 'checked');
                    break;
                case 'repeat-y':
                    this.$el.find('input.image_scaling[value=tile_vertically]').attr('checked', 'checked');
                    break;
                case 'repeat':
                    this.$el.find('input.image_scaling[value=tile]').attr('checked', 'checked');
                    break;
                default:
                    switch (node.style.backgroundSize) {
                        case 'cover':
                            this.$el.find('input.image_scaling[value=full_screen]').attr('checked', 'checked');
                            break;
                        case 'contain':
                            this.$el.find('input.image_scaling[value=fit]').attr('checked', 'checked');
                            break;
                        default:
                            this.$el.find('input.image_scaling[value=normal]').attr('checked', 'checked');
                    }
            }

            //  Chose current state of background position based on element
            switch (node.style.backgroundPosition) {
                case "100% 100%":
                    this.$el.find('input.image_position[value=bottom_right]').attr('checked', 'checked');
                    break;
                case "50% 100%":
                    this.$el.find('input.image_position[value=bottom_center]').attr('checked', 'checked');
                    break;
                case "0px 100%":
                    this.$el.find('input.image_position[value=bottom_left]').attr('checked', 'checked');
                    break;
                case "100% 50%":
                    this.$el.find('input.image_position[value=middle_right]').attr('checked', 'checked');
                    break;
                case "50% 50%":
                    this.$el.find('input.image_position[value=middle_center]').attr('checked', 'checked');
                    break;
                case "0px 50%":
                    this.$el.find('input.image_position[value=middle_left]').attr('checked', 'checked');
                    break;
                case "100% 0px":
                    this.$el.find('input.image_position[value=top_right]').attr('checked', 'checked');
                    break;
                case "50% 0px":
                    this.$el.find('input.image_position[value=top_center]').attr('checked', 'checked');
                    break;
                default:
                    this.$el.find('input.image_position[value=top_left]').attr('checked', 'checked');
                    break;
            }

            //  Chose current state of background attachment based on element
            if (node.style.backgroundAttachment == "scroll") {
                this.$el.find('input.page-setting-background-attachment').attr('checked', 'checked');
            }

            this.delegateEvents();
            return this;
        }
    });
});
