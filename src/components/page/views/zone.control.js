/**
 *
 *    ZONE CONTROL VIEW
 *    VERSION: 0.0.0
 *
 *    Defines the view for zone control such as resize and handels and stuff
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'interact'
], function($, _, Backbone, Template, Interact) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'page-zone-control',
        events: {
            'click': function(event) {
                $('.zone-active').removeClass('zone-active');
                $(this.parent).addClass('zone-active');
            }
        },
        initialize: function(options) {
            this.parent = options.parent;
        },
        render: function() {
            //  Render template
            this.$el.html(Template('page', 'zone_control', {}));

            //  Resize Handle
            Interact(this.$el.find('.page-zone-horizontal-resize-handle')[0])
                .inertia({
                    resistance: 15,
                    zeroResumeDelta: true
                })
                .draggable({
                    axis: 'y'
                })
                .on('dragmove', $.proxy(function(event) {
                    var newHeight = this.parent.offsetHeight + event.dy;
                    this.parent.style.height = newHeight + "px";

                    if (this.parent.offsetHeight + this.parent.offsetTop > $('body')[0].offsetHeight) {
                        $('body').scrollTop($(document).height() - $(window).height());
                    }

                    //  Wrapper extends if it size is less then new size of zone
                    if ($(this.parent).parents('.page-zone-wrapper')[0].offsetHeight < newHeight) {
                        $(this.parent).parents('.page-zone-wrapper')[0].style.height = newHeight + 'px';
                    }

                    //  Wrapper shrinks if it size is more than max size of zone in it
                    var max_height = 0;
                    $(this.parent).parents('.page-zone-wrapper').find('.page-zone').each(function(index, elm) {
                        if (elm.offsetHeight > max_height) {
                            max_height = elm.offsetHeight;
                        }
                    });
                    if ($(this.parent).parents('.page-zone-wrapper')[0].offsetHeight > max_height) {
                        $(this.parent).parents('.page-zone-wrapper')[0].style.height = newHeight + 'px';
                    }
                }, this))
                .on('dragend', $.proxy(function(event) {
                    $(this.parent).trigger('change');
                }, this));

            this.$el.find('.page-zone-horizontal-resize-handle').on('click contextmenu', function(event) {
                event.preventDefault();
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
            });

            //  Resize Handle
            Interact(this.$el.find('.page-zone-drag-handle')[0])
                .inertia({
                    resistance: 15,
                    zeroResumeDelta: true
                })
                .draggable({
                    axis: 'y'
                })
                .on('dragmove', $.proxy(function(event) {
                    //  Margin-top set
                    this.parent.style.marginTop = (parseInt(this.parent.style.marginTop || 0) + event.dy) + "px";

                    if (parseInt(this.parent.style.marginTop || 0) < 0) {
                        this.parent.style.marginTop = "0px";
                    }

                    var newHeight = $(this.parent).parents('.page-zone-wrapper')[0].offsetHeight + event.dy;

                    //  Wrapper extends if it size is less then new size of zone
                    if ($(this.parent).parents('.page-zone-wrapper')[0].offsetHeight < newHeight) {
                        $(this.parent).parents('.page-zone-wrapper')[0].style.height = newHeight + 'px';
                    }

                    //  Wrapper shrinks if it size is more than max size of zone in it
                    var max_height = 0;
                    $(this.parent).parents('.page-zone-wrapper').find('.page-zone').each(function(index, elm) {
                        if (elm.offsetHeight > max_height) {
                            max_height = elm.offsetHeight;
                        }
                    });
                    if ($(this.parent).parents('.page-zone-wrapper')[0].offsetHeight > max_height) {
                        $(this.parent).parents('.page-zone-wrapper')[0].style.height = newHeight + 'px';
                    }
                }, this))
                .on('dragend', $.proxy(function(event) {
                    $(this.parent).trigger('change');
                }, this));

            this.$el.find('.page-zone-drag-handle').on('click contextmenu', function(event) {
                event.preventDefault();
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
            });



            return this;
        }
    });
});
