/**
 *
 *    ZONE STYLE DIALOG VIEW
 *    VERSION: 0.0.0
 *
 *    Defines the view for zone styles
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'color_selector'
], function($, _, Backbone, Template, ColorSelector) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'modal-wrapper',
        events: {
            'click span.zone-setting-color-thumbnail': function(event) {
                ColorSelector('mini', $.proxy(function(color) {
                    //  Change thumbnail color
                    event.target.className = event.target.className.replace(/ bg_.+[\s]|bg_.+$/, ' ');
                    $(event.target).removeClass('color_transparent').addClass(color).addClass('bg_' + color).attr('data-color', color).attr('data-changed', 'true');

                    //  Change zone color
                    if ($(event.target).hasClass('zone-setting-background-color-thumbnail')) {
                        var background_node = $(this.target).parents('.page-zone-wrapper').find('.zone-background-node')[0];
                        background_node.className = background_node.className.replace(/ bg_.+[\s]|bg_.+$/, ' ');
                        $(background_node).addClass('bg_' + color);
                    }
                    else if ($(event.target).hasClass('zone-setting-background-center-color-thumbnail')) {
                        this.target.className = this.target.className.replace(/ bg_.+[\s]|bg_.+$/, ' ');
                        $(this.target).addClass('bg_' + color);
                    }
                    else {
                        this.target.className = this.target.className.replace(/ border_.+[\s]|border_.+$/, ' ');
                        $(this.target).addClass('border_' + color);
                    }
                }, this), event);
            },
            "change input.zone-setting-border-radius-input": function (event) {
                //  Change border radius thumbnail
                $(event.target).parent().find('span').css('border-' + $(event.target).attr('data-target') + '-radius', $(event.target).val()+"px");

                //  Change zone border radius
                $(this.target).css('border-' + $(event.target).attr('data-target') + '-radius', $(event.target).val()+"px");
            },
            'change input.zone-setting-border-width-input': function (event) {
                //  Change border width thumbnail
                $(event.target).parent().find('span').css('border-' + $(event.target).attr('data-target') + '-width', $(event.target).val()+"px");

                //  Change zone border radius
                $(this.target).css('border-' + $(event.target).attr('data-target') + '-width', $(event.target).val()+"px");
            },
            'change select.zone-setting-border-style-select': function (event) {
                $(this.target).css('border-style', $(event.target).val());
            }

        },
        initialize: function(options) {
            this.target = options.target;
        },
        render: function() {
            //  Extract Zone Background color
            var background_node = $(this.target).parents('.page-zone-wrapper').find('.zone-background-node')[0],
                zone_background_color = "color_transparent";
            _.each(background_node.className.split(/\s+/), function(className) {
                if (/^bg_.+$/.test(className)) {
                    zone_background_color = className;
                }
            }, this);

            //  Extract Zone Background center color
            var zone_background_center_color = "color_transparent";
            _.each(this.target.className.split(/\s+/), function(className) {
                if (/^bg_.+$/.test(className)) {
                    zone_background_center_color = className;
                }
            }, this);

            //  Extract Zone Border color
            var zone_border_color = "color_transparent";
            _.each(this.target.className.split(/\s+/), function(className) {
                if (/^border_.+$/.test(className)) {
                    zone_border_color = "bg" + className.substring(6);
                }
            }, this);

            //  Extract Zone Border radius
            var zone_border_radius_bottom_left = this.target.style.borderBottomLeftRadius || 0,
                zone_border_radius_bottom_right = this.target.style.borderBottomRightRadius || 0,
                zone_border_radius_top_left = this.target.style.borderTopLeftRadius || 0,
                zone_border_radius_top_right = this.target.style.borderTopRightRadius || 0;

            //  Extract Zone Border width
            var zone_border_radius_bottom = this.target.style.borderBottomWidth || 0,
                zone_border_radius_right = this.target.style.borderRightWidth || 0,
                zone_border_radius_top = this.target.style.borderTopWidth || 0,
                zone_border_radius_left = this.target.style.borderLeftWidth || 0;

            //  Constructing the template
            this.$el.html(Template('page', 'zone_style', {
                zone_background_color: zone_background_color,
                zone_background_center_color: zone_background_center_color,
                zone_border_color: zone_border_color,
                zone_border_radius_top_left: zone_border_radius_top_left,
                zone_border_radius_top_right: zone_border_radius_top_right,
                zone_border_radius_bottom_left: zone_border_radius_bottom_left,
                zone_border_radius_bottom_right: zone_border_radius_bottom_right,
                zone_border_radius_bottom: zone_border_radius_bottom,
                zone_border_radius_right: zone_border_radius_right,
                zone_border_radius_top: zone_border_radius_top,
                zone_border_radius_left: zone_border_radius_left,
                zone_border: $(this.target).css('border'),
            }));
            return this;
        }
    });
});
