
define([
	'jquery',
	'underscore',
	'backbone',
	'templates',
	'widget',
	'src/components/text/views/text'
], function ($, _, Backbone, Templates, Widget, View) {
	return Widget.extend({
		contentView: new View()
	});
});