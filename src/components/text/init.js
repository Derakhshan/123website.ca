/**
 *		TEXT WIDGET
 *		VERSION: 0.0.1
 *
 * 		This widget manges all text based contents in editor.
 *
 * 		@dependencies:
 * 			Widget 	Based on base-widget class
 * 		@views
 * 			Text 	A Backbone View, manages the main content of widget
 * 			Setting A Backbone View, manages the main settings of text widget
 * 		@models
 * 			Main 	A Backbone model, containing main features of text widget
 *
 */

define([
    'jquery',
    'underscore',
    'widget',
    'tinymce',
    'interact',
    'color_selector',
    'text/views/text',
    'text/views/setting',
    'text/models/text',
    'text/views/editorPanel',

], function(jQuery, _, Widget, tinymce, Interact, ColorSelector, Text, Setting, Model, Panel) {
    //	Private Methods
    function editMode(event) {
        //	Lock and undeligate widget
        this.undelegateEvents();
        Interact(this.el).draggable(false);
        this.$el.find('.widget-resize-handle').css('display', 'none');
        var context = this;

        //	Block click and context menu propagation
        this.$el.on('click contextmenu', function(event) {
            event.cancelBubble = true;
            if (event.stopPropagation) {
                event.stopPropagation();
            }
        });

        //	Set an id on element in order to pass the id to tinymce
        var id = _.uniqueId('editor_');
        this.$el.find('.text-widget-wrapper').attr({
            id: id,
            contentEditable: "true"
        });

        //	Attach the panel container to the body
        var x = this.el.offsetParent.offsetLeft + this.el.offsetLeft,
            y = this.el.offsetParent.offsetTop + this.el.offsetTop,
            w = this.el.offsetWidth;
        $('body').append((new Panel(id, x, y, w)).render().$el);

        //	focus on editing
        //	when editor unfocused close edit-mode
        //	model change
        tinymce.init({
            skin_url: '/editor/app/src/components/text/assets/editor/theme',
            fixed_toolbar_container: "#panel_" + id,
            setup: function(editor) {
                editor.on('blur', function(event) {
                    $("#panel_" + id).parent().css('display', 'none');
                });
                editor.on('focus', function(event) {
                    $("#panel_" + id).parent().css('display', 'block');
                });
                editor.addButton('done', {
                    icon: 'done',
                    title: 'Save Changes',
                    onclick: $.proxy(function(event) {
                        $("#panel_editor_18").parent().remove();
                        this.$el.find('.text-widget-wrapper').removeAttr('contentEditable');
                        var settings = _.clone(this.model.get('settings'));
                        settings.content = editor.getContent();
                        this.model.set({
                            settings: settings
                        }, {
                            validate: true
                        });
                        this.render();
                    }, context)
                });
                editor.addButton('fontColorSelector', {
                    icon: 'font-color',
                    title: 'Text color',
                    onclick: function(event) {
                        ColorSelector('mini', function(color) {
                            //	Selected node
                            var node = editor.selection.getNode();

                            //	Remove background format
                            node.className = node.className.replace(/ color_.+[\s]|color_.+$/, ' ');

                            //	Add new format
                            if (color != 'color_transparent') {
                                editor.formatter.apply(color);
                                $('.mce-i-font-color')[0].className = $('.mce-i-font-color')[0].className.replace(/ color_.+[\s]|color_.+$/, ' ');
                                $('.mce-i-font-color').addClass(color);
                            } else {
                                $('.mce-i-font-color')[0].className = $('.mce-i-font-color')[0].className.replace(/ color_.+[\s]|color_.+$/, ' ');
                            }

                            //editor.focus();
                        }, event);
                    }
                });
                editor.addButton('fontBackgroundColorSelector', {
                    icon: 'font-background',
                    title: 'Text background color',
                    onclick: function(event) {
                        ColorSelector('mini', function(color) {
                            //	Selected node
                            var node = editor.selection.getNode();

                            //	Remove background format
                            _.each(node.className.split(/\s+/), function(className) {
                                if (/^bg_.+$/.test(className)) {
                                    $(node).removeClass(className);
                                }
                            });

                            //	Add Background format
                            if (color != 'color_transparent') {
                                editor.formatter.apply("bg_" + color);
                                $('.mce-i-font-background')[0].className = $('.mce-i-font-background')[0].className.replace(/ bg_.+[\s]|bg_.+$/, ' ');
                                $('.mce-i-font-background').addClass('bg_' + color);
                            } else {
                                $('.mce-i-font-background')[0].className = $('.mce-i-font-background')[0].className.replace(/ bg_.+[\s]|bg_.+$/, ' ');
                            }

                            //editor.focus();
                        }, event);
                    }
                });
            },
            formats: {
                color_1: {
                    inline: "span",
                    classes: "color_1"
                },
                color_2: {
                    inline: "span",
                    classes: "color_2"
                },
                color_3: {
                    inline: "span",
                    classes: "color_3"
                },
                color_4: {
                    inline: "span",
                    classes: "color_4"
                },
                color_5: {
                    inline: "span",
                    classes: "color_5"
                },
                color_6: {
                    inline: "span",
                    classes: "color_6"
                },
                color_7: {
                    inline: "span",
                    classes: "color_7"
                },
                color_8: {
                    inline: "span",
                    classes: "color_8"
                },
                color_9: {
                    inline: "span",
                    classes: "color_9"
                },
                color_10: {
                    inline: "span",
                    classes: "color_10"
                },
                color_11: {
                    inline: "span",
                    classes: "color_11"
                },
                color_12: {
                    inline: "span",
                    classes: "color_12"
                },
                color_13: {
                    inline: "span",
                    classes: "color_13"
                },
                color_14: {
                    inline: "span",
                    classes: "color_14"
                },
                color_15: {
                    inline: "span",
                    classes: "color_15"
                },
                color_16: {
                    inline: "span",
                    classes: "color_16"
                },
                color_17: {
                    inline: "span",
                    classes: "color_17"
                },
                color_18: {
                    inline: "span",
                    classes: "color_18"
                },
                color_19: {
                    inline: "span",
                    classes: "color_19"
                },
                color_20: {
                    inline: "span",
                    classes: "color_20"
                },
                color_21: {
                    inline: "span",
                    classes: "color_21"
                },
                color_22: {
                    inline: "span",
                    classes: "color_22"
                },
                color_23: {
                    inline: "span",
                    classes: "color_23"
                },
                color_24: {
                    inline: "span",
                    classes: "color_24"
                },
                color_25: {
                    inline: "span",
                    classes: "color_25"
                },
                color_26: {
                    inline: "span",
                    classes: "color_26"
                },
                color_27: {
                    inline: "span",
                    classes: "color_27"
                },
                color_28: {
                    inline: "span",
                    classes: "color_28"
                },
                color_29: {
                    inline: "span",
                    classes: "color_29"
                },
                color_30: {
                    inline: "span",
                    classes: "color_30"
                },
                color_31: {
                    inline: "span",
                    classes: "color_31"
                },
                color_32: {
                    inline: "span",
                    classes: "color_32"
                },
                color_33: {
                    inline: "span",
                    classes: "color_33"
                },
                color_34: {
                    inline: "span",
                    classes: "color_34"
                },
                color_35: {
                    inline: "span",
                    classes: "color_35"
                },
                bg_color_1: {
                    inline: "span",
                    classes: "bg_color_1"
                },
                bg_color_2: {
                    inline: "span",
                    classes: "bg_color_2"
                },
                bg_color_3: {
                    inline: "span",
                    classes: "bg_color_3"
                },
                bg_color_4: {
                    inline: "span",
                    classes: "bg_color_4"
                },
                bg_color_5: {
                    inline: "span",
                    classes: "bg_color_5"
                },
                bg_color_6: {
                    inline: "span",
                    classes: "bg_color_6"
                },
                bg_color_7: {
                    inline: "span",
                    classes: "bg_color_7"
                },
                bg_color_8: {
                    inline: "span",
                    classes: "bg_color_8"
                },
                bg_color_9: {
                    inline: "span",
                    classes: "bg_color_9"
                },
                bg_color_10: {
                    inline: "span",
                    classes: "bg_color_10"
                },
                bg_color_11: {
                    inline: "span",
                    classes: "bg_color_11"
                },
                bg_color_12: {
                    inline: "span",
                    classes: "bg_color_12"
                },
                bg_color_13: {
                    inline: "span",
                    classes: "bg_color_13"
                },
                bg_color_14: {
                    inline: "span",
                    classes: "bg_color_14"
                },
                bg_color_15: {
                    inline: "span",
                    classes: "bg_color_15"
                },
                bg_color_16: {
                    inline: "span",
                    classes: "bg_color_16"
                },
                bg_color_17: {
                    inline: "span",
                    classes: "bg_color_17"
                },
                bg_color_18: {
                    inline: "span",
                    classes: "bg_color_18"
                },
                bg_color_19: {
                    inline: "span",
                    classes: "bg_color_19"
                },
                bg_color_20: {
                    inline: "span",
                    classes: "bg_color_20"
                },
                bg_color_21: {
                    inline: "span",
                    classes: "bg_color_21"
                },
                bg_color_22: {
                    inline: "span",
                    classes: "bg_color_22"
                },
                bg_color_23: {
                    inline: "span",
                    classes: "bg_color_23"
                },
                bg_color_24: {
                    inline: "span",
                    classes: "bg_color_24"
                },
                bg_color_25: {
                    inline: "span",
                    classes: "bg_color_25"
                },
                bg_color_26: {
                    inline: "span",
                    classes: "bg_color_26"
                },
                bg_color_27: {
                    inline: "span",
                    classes: "bg_color_27"
                },
                bg_color_28: {
                    inline: "span",
                    classes: "bg_color_28"
                },
                bg_color_29: {
                    inline: "span",
                    classes: "bg_color_29"
                },
                bg_color_30: {
                    inline: "span",
                    classes: "bg_color_30"
                },
                bg_color_31: {
                    inline: "span",
                    classes: "bg_color_31"
                },
                bg_color_32: {
                    inline: "span",
                    classes: "bg_color_32"
                },
                bg_color_33: {
                    inline: "span",
                    classes: "bg_color_33"
                },
                bg_color_34: {
                    inline: "span",
                    classes: "bg_color_34"
                },
                bg_color_35: {
                    inline: "span",
                    classes: "bg_color_35"
                },
            },
            style_formats: [{
                title: 'Title',
                block: 'h1',
                classes: "font_title"
            }, {
                title: 'Page Title',
                block: 'h2',
                classes: "font_page_title"
            }, {
                title: 'Heading XL',
                block: 'h3',
                classes: "font_heading_xl"
            }, {
                title: 'Heading L',
                block: 'h4',
                classes: "font_heading_l"
            }, {
                title: 'Heading M',
                block: 'h5',
                classes: "font_heading_m"
            }, {
                title: 'Heading S',
                block: 'h6',
                classes: "font_heading_s"
            }, {
                title: 'Body L',
                block: 'p',
                classes: "font_body_l"
            }, {
                title: 'Body M',
                block: 'p',
                classes: "font_body_m"
            }, {
                title: 'Body S',
                block: 'p',
                classes: "font_body_s"
            }, {
                title: 'Body XS',
                block: 'p',
                classes: "font_body_xs"
            }, ],
            font_formats: _.map(application.settings.fonts.systemFonts, function(font) {
                return font.name + "=" + font.family
            }).concat(_.map(application.settings.fonts.googleFonts, function(font) {
                return font.name + "=" + font.family
            })).join(';'),
            selector: "div.text-widget-wrapper#" + id,
            inline: true,
            plugins: [
                "directionality",
                "link",
            ],
            toolbar: [
                "styleselect | fontselect fontsizeselect | bold italic underline | fontColorSelector fontBackgroundColorSelector",
                "removeformat | outdent indent ltr rtl | bullist numlist | alignleft aligncenter alignright alignjustify | link unlink | done"
            ],
            menubar: false,
            statusbar: false,
            auto_focus: id
        });
    }

    return Widget.extend({
        contentFactory: Text,
        modelFactory: Model,
        resizeable: 'xy',
        events: {
            'dblclick': editMode
        },
        contextMenu: [{
            id: 'edit',
            func: editMode,
            icon: 'fa fa-edit',
            label: 'Edit Text'
        }, {
            id: 'setting',
            func: function() {
                //	Freez the widget
                Interact(this.el).draggable(false);
                this.undelegateEvents();

                var save = $.proxy(function() {
                    this.model.set({
                        position_left: layout.$el.find('#left').val(),
                        position_top: layout.$el.find('#top').val(),
                        width: layout.$el.find('#width').val(),
                        height: layout.$el.find('#height').val(),
                        show_on_all_pages: layout.$el.find('#show_on_all_pages').prop('checked')
                    }, {
                        validate: true
                    });
                    layout.$el.dialog('close');
                }, this);

                var layout = new Setting({
                    model: this.model,
                });

                layout.events['click .text-setting-edit-text-btn'] = $.proxy(function(event) {
                    console.log('test');
                    $.proxy(editMode, this)(event);
                }, this);

                layout.events['click .text-setting-add-animation-btn'] = $.proxy(function(event) {
                    $.proxy(this.addAnimation, this)(event);
                }, this);

                layout.events['change #left'] = $.proxy(function(event) {
                    this.$el.css('left', layout.$el.find('#left').val() + "px");
                }, this);

                layout.events['change #top'] = $.proxy(function(event) {
                    this.$el.css('top', layout.$el.find('#top').val() + "px");
                }, this);

                layout.events['change #width'] = $.proxy(function(event) {
                    this.$el.css('width', layout.$el.find('#width').val() + "px");
                }, this);

                layout.events['change #height'] = $.proxy(function(event) {
                    this.$el.css('height', layout.$el.find('#height').val() + "px");
                }, this);

                layout.render().$el.dialog({
                    title: "Text Settings",
                    width: 350,
                    height: 566,
                    closeOnEscape: true,
                    buttons: [{
                        text: "Save",
                        click: save
                    }],
                    close: $.proxy(function(event) {
                        this.render();
                        layout.$el.dialog('destroy');
                    }, this)
                });
            },
            icon: 'fa fa-cog',
            label: 'Settings'
        }],
    });
});
