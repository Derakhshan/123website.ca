/**
 *
 *    TEXT WIDGET EDITOR PANEL
 *    VERSION: 0.0.0
 *
 *    Defines the view for editor's panel
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'interact'
], function($, _, Backbone, Template, Interact) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'text-editor-panel-wrapper',
        events: {
            'contextmenu': function(event) {
                //  Prevent context menu propagation
                event.preventDefault();
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
            },
        },
        initialize: function(id, x, y, w) {
            this.id = id;

            if ((y - 70) < 0) {
                this.pos_y = 0;
            } else {
                this.pos_y = y - 70;
            }

            if (x < 0) {
                this.pos_x = 0;
            } else if ((window.innerWidth - x) < 555) {
                if ((x + w) > window.innerWidth) {
                    this.pos_x = window.innerWidth - 555;
                } else {
                    this.pos_x = (x + w) - 555;
                }
            } else {
                this.pos_x = x;
            }
        },
        render: function() {
            //  Constructing the template
            this.$el.html(Template('text', 'editor_panel', {
                id: this.id
            }));

            this.$el.css({
                top: this.pos_y + 'px',
                left: this.pos_x + 'px'
            });

            this.$el.attr('data-x', this.pos_x);
            this.$el.attr('data-y', this.pos_y);

            //  Draggable
            Interact(this.el)
                .inertia({
                    resistance: 15,
                    zeroResumeDelta: true
                })
                .draggable({
                    allowFrom: this.$el.find('.text-editor-panel-wrapper-handle')[0],
                    onmove: function(event) {
                        var target = event.target,
                            x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
                            y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

                        target.style.left = x + "px";
                        target.setAttribute('data-x', x);
                        target.style.top = y + "px";
                        target.setAttribute('data-y', y);
                    },
                });

            return this;
        }
    });
});
