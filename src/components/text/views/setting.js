/**
 *
 *    TEXT WIDGET SETTING MODAL
 *    VERSION: 0.0.0
 *
 *    Defines the setting model for text widget
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
], function($, _, Backbone, Template) {
    return Backbone.View.extend({
        tagName: 'div',
        className: 'modal-wrapper',
        events: {
            'contextmenu': function(event) {
                event.preventDefault();
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
            },
        },
        initialize: function() {

        },
        render: function() {
            //  Constructing the template
            this.$el.html(Template('text', 'setting', this.model.toJSON()));

            this.delegateEvents();
            return this;
        }
    });
});
