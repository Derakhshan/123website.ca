/**
 *
 * 		TEXT VIEW
 * 		VERSION: 0.0.0
 *
 *		Defines the main view for text widgets
 */


define([
	'jquery',
	'underscore',
	'backbone',
	'templates'
], function ($, _, Backbone, Template) {
	return Backbone.View.extend({
		tagName: 'div',
		className: 'text-widget-wrapper',
		initialize: function () {
			this.render();
		},
		render: function () {
			this.$el.html(Template('text', 'text', {content: this.model.get('settings').content}));
			this.$el.css({
				width: this.model.get('width') + "px",
				height: this.model.get('height') + "px",
				'z-index': this.model.get('settings').z_index
			});

			return this;
		}
	});
});