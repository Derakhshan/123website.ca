// app/src/helpers/templates/init.js

///
//
//	Templates
//		This helper class creates and manages the template system, isolating it from rest of application for possible
//		changes in templates system.
//		Version: 0.2
//		Syntax: mustache
//		Compiler: Hogan
//		parameters:
//			URI: A string, URI to template
//			Options: A dictionary, containing replacement to provide underscore templates
//			Callback: A function, template will be passed to this option after being fetched and compiled
//		return:
//			Error: If template was not able to load URI
//
///

define([], function (){
	return function (app, template, parameters) {
		return application.templates[app + '_' + template].render(parameters);
	}
		
});